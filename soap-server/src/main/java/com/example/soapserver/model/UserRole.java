package com.example.soapserver.model;

public enum UserRole {
    PATIENT,
    CAREGIVER,
    DOCTOR;
}
