package com.example.soapserver.model;

public interface IdNameEntity {
    Long getEntityId();

    String getEntityName();
}
