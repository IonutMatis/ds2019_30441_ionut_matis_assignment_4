package com.example.soapserver.model;

import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "activities")
public class Activity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String activity;
    private long patient_id;
    private long start;
    private long end;

    @Type(type = "yes_no")
    private boolean anomalous;

    @Type(type = "yes_no")
    @Column(name = "normal_behaviour")
    private Boolean normalBehaviour;

    private String recommendation;
}
