package com.example.soapserver.utils;

import java.time.format.DateTimeFormatter;

public class MyUtils {
    public static final String NAMESPACE_URI = "http://example.com/soapserver/soap";
    public final static DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
}
