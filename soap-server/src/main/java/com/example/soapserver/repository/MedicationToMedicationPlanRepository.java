package com.example.soapserver.repository;

import com.example.soapserver.model.MedicationToMedicationPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicationToMedicationPlanRepository extends JpaRepository<MedicationToMedicationPlan, Long> {
}