package com.example.soapserver.repository;

import com.example.soapserver.model.MedicationHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicationHistoryRepository extends JpaRepository<MedicationHistory, Long> {
}
