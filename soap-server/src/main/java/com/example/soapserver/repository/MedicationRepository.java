package com.example.soapserver.repository;

import com.example.soapserver.model.Medication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MedicationRepository extends JpaRepository<Medication, Long> {
    @Query("SELECT med FROM Medication med WHERE med.name = :name")
    Optional<Medication> findByName(@Param("name") String medicationName);
}
