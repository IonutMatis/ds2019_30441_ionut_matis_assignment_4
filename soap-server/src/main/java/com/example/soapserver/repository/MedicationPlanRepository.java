package com.example.soapserver.repository;

import com.example.soapserver.model.MedicationPlan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicationPlanRepository extends JpaRepository<MedicationPlan, Long> {
}