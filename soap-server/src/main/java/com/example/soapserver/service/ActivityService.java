package com.example.soapserver.service;

import com.example.soapserver.exception.UnknownActivityException;
import com.example.soapserver.model.Activity;
import com.example.soapserver.repository.ActivityRepository;
import com.example.soapserver.soap.SOAPSimplePatientActivity;
import com.example.soapserver.soap.UpdateActivityRequest;
import com.example.soapserver.soap.UpdateActivityResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ActivityService {
    private final ActivityRepository activityRepository;

    public UpdateActivityResponse updateActivity(UpdateActivityRequest request) {
        UpdateActivityResponse response = new UpdateActivityResponse();
        SOAPSimplePatientActivity requestActivity = request.getUpdatedActivity();

        Activity foundActivity = activityRepository.findById(requestActivity.getId())
                .orElseThrow(UnknownActivityException::new);

        foundActivity.setNormalBehaviour(requestActivity.isNormalBehaviour());
        foundActivity.setRecommendation(requestActivity.getRecommendation());

        activityRepository.save(foundActivity);

        return response;
    }

}
