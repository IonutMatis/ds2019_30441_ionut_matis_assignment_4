package com.example.soapserver.service;

import com.example.soapserver.model.Doctor;
import com.example.soapserver.repository.DoctorRepository;
import com.example.soapserver.soap.LoginRequest;
import com.example.soapserver.soap.LoginResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@RequiredArgsConstructor
@Service
public class LoginService {
    private final PasswordEncoder passwordEncoder;
    private final DoctorRepository doctorRepository;

    public LoginResponse login(LoginRequest request) {
        LoginResponse response = new LoginResponse();

        String username = request.getUsername();
        String password = request.getPassword();

        Optional<Doctor> foundDoctor = doctorRepository.findByName(username);
        if (foundDoctor.isPresent()) {
            if (passwordEncoder.matches(password, foundDoctor.get().getPassword())) {
                response.setStatus(200);
                response.setDoctorName(username);

                return response;
            }
        }

        response.setStatus(403);
        response.setDoctorName("invalid");
        return response;
    }
}
