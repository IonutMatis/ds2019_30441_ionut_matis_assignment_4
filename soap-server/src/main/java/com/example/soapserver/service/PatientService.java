package com.example.soapserver.service;

import com.example.soapserver.converter.ActivityConverter;
import com.example.soapserver.converter.MedicationHistoryConverter;
import com.example.soapserver.converter.PatientConverter;
import com.example.soapserver.exception.UnknownMedicationPlanException;
import com.example.soapserver.model.MedicationPlan;
import com.example.soapserver.repository.ActivityRepository;
import com.example.soapserver.repository.MedicationHistoryRepository;
import com.example.soapserver.repository.MedicationPlanRepository;
import com.example.soapserver.repository.PatientRepository;
import com.example.soapserver.soap.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class PatientService {
    private final PatientRepository patientRepository;
    private final ActivityRepository activityRepository;
    private final MedicationHistoryRepository medicationHistoryRepository;
    private final MedicationPlanRepository medicationPlanRepository;


    public GetAllSimplePatientsResponse getSimplePatients(GetAllSimplePatientsRequest request) {
        GetAllSimplePatientsResponse response = new GetAllSimplePatientsResponse();

        patientRepository.findAll()
                .forEach(patient -> {
                    SOAPSimplePatient simplePatient = PatientConverter.generateSOAP(patient);
                    response.getSimplePatientList().add(simplePatient);
                });

        return response;
    }

    public GetPatientActivityHistoryResponse getPatientHistory(GetPatientActivityHistoryRequest request) {
        GetPatientActivityHistoryResponse response = new GetPatientActivityHistoryResponse();

        long patientId = request.getPatientId();
        activityRepository.findAll().stream()
                .filter(activity -> activity.getPatient_id() == patientId)
                .forEach(activity -> {
                    SOAPPatientActivity soapActivity = ActivityConverter.generateSOAP(activity);
                    response.getPatientActivityList().add(soapActivity);
                });

        return response;
    }

    public GetPatientMedicationHistoryResponse getPatientMedication(GetPatientMedicationHistoryRequest request) {
        GetPatientMedicationHistoryResponse response = new GetPatientMedicationHistoryResponse();

        long patientId = request.getPatientId();
        medicationHistoryRepository.findAll().stream()
                .filter(medicationHistory -> {
                    long medPlanId = medicationHistory.getMedicationPlanId();
                    MedicationPlan medicationPlan = medicationPlanRepository.findById(medPlanId)
                            .orElseThrow(UnknownMedicationPlanException::new);
                    return medicationPlan.getPatient().getId().equals(patientId);
                })
                .forEach(medicationHistory -> {
                    SOAPPatientMedication soapMedication = MedicationHistoryConverter.generateSOAP(medicationHistory);
                    response.getPatientMedicationList().add(soapMedication);
                });

        return response;
    }
}
