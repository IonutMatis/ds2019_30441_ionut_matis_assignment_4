package com.example.soapserver.endpoint;

import com.example.soapserver.service.ActivityService;
import com.example.soapserver.service.PatientService;
import com.example.soapserver.soap.*;
import com.example.soapserver.utils.MyUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
@RequiredArgsConstructor
public class PatientEndpoint {
    private final static String GET_ALL_SIMPLE_PATIENT_REQUEST = "getAllSimplePatientsRequest";
    private final static String GET_PATIENT_ACTIVITY_REQUEST = "getPatientActivityHistoryRequest";
    private final static String GET_PATIENT_MEDICATION_REQUEST = "getPatientMedicationHistoryRequest";
    private final static String UPDATE_PATIENT_ACTIVITY_REQUEST = "updateActivityRequest";

    private final ActivityService activityService;
    private final PatientService patientService;

    @PayloadRoot(namespace = MyUtils.NAMESPACE_URI, localPart = GET_ALL_SIMPLE_PATIENT_REQUEST)
    @ResponsePayload
    public GetAllSimplePatientsResponse getSimplePatients(@RequestPayload GetAllSimplePatientsRequest request) {
        return patientService.getSimplePatients(request);
    }

    @PayloadRoot(namespace = MyUtils.NAMESPACE_URI, localPart = GET_PATIENT_ACTIVITY_REQUEST)
    @ResponsePayload
    public GetPatientActivityHistoryResponse getPatientHistory(@RequestPayload GetPatientActivityHistoryRequest request) {
        return patientService.getPatientHistory(request);
    }

    @PayloadRoot(namespace = MyUtils.NAMESPACE_URI, localPart = GET_PATIENT_MEDICATION_REQUEST)
    @ResponsePayload
    public GetPatientMedicationHistoryResponse getPatientMedication(@RequestPayload GetPatientMedicationHistoryRequest request) {
        return patientService.getPatientMedication(request);
    }

    @PayloadRoot(namespace = MyUtils.NAMESPACE_URI, localPart = UPDATE_PATIENT_ACTIVITY_REQUEST)
    @ResponsePayload
    public UpdateActivityResponse updateActivity(@RequestPayload UpdateActivityRequest request) {
        return activityService.updateActivity(request);
    }
}
