package com.example.soapserver.endpoint;

import com.example.soapserver.service.LoginService;
import com.example.soapserver.soap.LoginRequest;
import com.example.soapserver.soap.LoginResponse;
import com.example.soapserver.utils.MyUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
@RequiredArgsConstructor
public class LoginEndpoint {
    private final LoginService loginService;

    @PayloadRoot(namespace = MyUtils.NAMESPACE_URI, localPart = "loginRequest")
    @ResponsePayload
    public LoginResponse login(@RequestPayload LoginRequest request) {
        return loginService.login(request);
    }
}
