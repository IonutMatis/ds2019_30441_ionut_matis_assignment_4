package com.example.soapserver.converter;

import com.example.soapserver.model.Patient;
import com.example.soapserver.soap.SOAPSimplePatient;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PatientConverter {
    public static SOAPSimplePatient generateSOAP(Patient patient) {
        SOAPSimplePatient soap = new SOAPSimplePatient();
        soap.setId(patient.getId());
        soap.setName(patient.getName());

        return soap;
    }
}
