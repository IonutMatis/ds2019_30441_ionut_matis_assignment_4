package com.example.soapserver.converter;

import com.example.soapserver.model.Activity;
import com.example.soapserver.soap.SOAPPatientActivity;
import com.example.soapserver.utils.MyUtils;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class ActivityConverter {
    public static SOAPPatientActivity generateSOAP(Activity activity) {
        SOAPPatientActivity soap = new SOAPPatientActivity();

        soap.setId(activity.getId());
        soap.setPatientId(activity.getPatient_id());
        soap.setActivityName(activity.getActivity());
        soap.setAnomalous(activity.isAnomalous());
        LocalDateTime startTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(activity.getStart()), ZoneId.systemDefault());
        soap.setStart(MyUtils.DATE_TIME_FORMATTER.format(startTime));
        LocalDateTime endTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(activity.getEnd()), ZoneId.systemDefault());
        soap.setEnd(MyUtils.DATE_TIME_FORMATTER.format(endTime));

        if (activity.getNormalBehaviour() == null) {
            soap.setNormalBehaviour(false);
            soap.setRecommendation("null");
        } else {
            soap.setNormalBehaviour(activity.getNormalBehaviour());
            if (activity.getRecommendation() == null) {
                soap.setRecommendation("null");
            } else {
                soap.setRecommendation(activity.getRecommendation());
            }
        }


        return soap;
    }
}
