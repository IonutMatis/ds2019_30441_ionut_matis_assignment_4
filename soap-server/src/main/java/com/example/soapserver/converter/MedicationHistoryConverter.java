package com.example.soapserver.converter;

import com.example.soapserver.model.MedicationHistory;
import com.example.soapserver.soap.SOAPPatientMedication;
import com.example.soapserver.utils.MyUtils;

public class MedicationHistoryConverter {
    public static SOAPPatientMedication generateSOAP(MedicationHistory medicationHistory) {
        SOAPPatientMedication soap = new SOAPPatientMedication();

        soap.setMedicationPlanId(medicationHistory.getMedicationPlanId());
        soap.setMedicationName(medicationHistory.getMedicationName());
        soap.setTaken(medicationHistory.isTaken());
        soap.setTime(MyUtils.DATE_TIME_FORMATTER.format(medicationHistory.getTime()));

        return soap;
    }
}
