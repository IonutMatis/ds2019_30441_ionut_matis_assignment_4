import medicationModel from "../model/medicationModel";

class MedicationPresenter {
    onCreate() {
        medicationModel.addMedication()
            .then(() => {
                medicationModel.resetNewMedicationProperties();
                window.location.assign("#/medications");
            })
    }

    onEdit() {
        medicationModel.editMedication()
            .then(() => {
                window.location.assign("#/medications");
            })
    }

    onDelete(medicationId) {
        medicationModel.deleteMedication(medicationId);
    }

    onChange(property, value) {
        medicationModel.changeNewMedicationProperty(property, value);
    };

    onEditChange(property, value) {
        medicationModel.changeEditedMedicationProperty(property, value);
    };

    onInit = () => {
        medicationModel.loadMedications();
    }
}

const medicationPresenter = new MedicationPresenter();

export default medicationPresenter;
