import medicationPlanModel from "../model/medicationPlanModel";

class MedicationPlanPresenter {
    onCreate() {
        medicationPlanModel.addMedicationPlan()
            .then(() => {
                medicationPlanModel.resetNewMedicationPlanProperties();
                window.location.assign("#/medication-plans");
            })
    }

    onEdit() {
        medicationPlanModel.editMedicationPlan()
            .then(() => {
                window.location.assign("#/medication-plans");
            })
    }

    onDelete(medicationPlanId) {
        medicationPlanModel.deleteMedicationPlan(medicationPlanId);
    }

    onChange(property, value) {
        medicationPlanModel.changeNewMedicationPlanProperty(property, value);
    };

    onEditChange(property, value) {
        medicationPlanModel.changeEditedMedicationPlanProperty(property, value);
    };

    onInit() {
        return medicationPlanModel.loadMedicationPlans();
    }
}

const medicationPlanPresenter = new MedicationPlanPresenter();

export default medicationPlanPresenter;
