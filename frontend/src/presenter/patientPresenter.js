import patientModel from "../model/patientModel";

class PatientPresenter {
    onCreate() {
        patientModel.addPatient()
            .then(() => {
                patientModel.resetNewPatientProperties();
                window.location.assign("#/patients");
            })
    }

    onEdit() {
        patientModel.editPatient()
            .then(() => {
                window.location.assign("#/patients");
            })
    }

    onDelete(patientId) {
        patientModel.deletePatient(patientId);
    }

    onChange(property, value) {
        patientModel.changeNewPatientProperty(property, value);
    };

    onEditChange(property, value) {
        patientModel.changeEditedPatientProperty(property, value);
    };

    onInit() {
        return patientModel.loadPatients();
    };

    onLoadPatient() {
        return patientModel.loadPatient();
    }

    onLoadMedicationPlans() {
        return patientModel.loadMedicationPlans();
    }
}

const patientPresenter = new PatientPresenter();

export default patientPresenter;