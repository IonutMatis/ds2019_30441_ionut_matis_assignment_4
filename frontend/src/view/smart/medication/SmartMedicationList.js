import React, {Component} from "react";
import medicationModel from "../../../model/medicationModel";
import MedicationList from "../../dumb/medication/MedicationList";
import medicationPresenter from "../../../presenter/medicationPresenter";
import userModel from "../../../model/userModel";


const mapModelStateToComponentState = (medicationModelState) => ({
    medications: medicationModelState.medications
});

export default class SmartMedicationList extends Component {
    constructor(props) {
        super(props);
        medicationPresenter.onInit();
        this.state = mapModelStateToComponentState(medicationModel.state);
        this.listener = () => this.setState(mapModelStateToComponentState(medicationModel.state));
        medicationModel.addListener("changeMedication", this.listener);
    }

    componentWillUnmount() {
        medicationModel.removeListener("changeMedication", this.listener);
    }

    render() {
        return (
            <MedicationList userModelState={userModel.state}
                            medications={this.state.medications}
                            onCreate={medicationPresenter.onCreate}
                            onDelete={medicationPresenter.onDelete}/>
        );
    }
}