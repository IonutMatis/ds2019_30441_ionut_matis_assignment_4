import React, {Component} from "react";
import medicationPlanModel from "../../../model/medicationPlanModel";
import MedicationPlanList from "../../dumb/medicationPlan/MedicationPlanList";
import medicationPlanPresenter from "../../../presenter/medicationPlanPresenter";
import userModel from "../../../model/userModel";
import patientPresenter from "../../../presenter/patientPresenter";


const mapModelStateToComponentState = (medicationPlanModelState) => ({
    medicationPlans: medicationPlanModelState.medicationPlans
});

export default class SmartMedicationPlanList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            medicationPlans: []
        };
        patientPresenter.onInit();
        medicationPlanPresenter.onInit()
            .then(() => {
                this.state = mapModelStateToComponentState(medicationPlanModel.state);
            });
        this.listener = () => this.setState(mapModelStateToComponentState(medicationPlanModel.state));
        medicationPlanModel.addListener("changeMedicationPlan", this.listener);
    }

    componentWillUnmount() {
        medicationPlanModel.removeListener("changeMedicationPlan", this.listener);
    }

    render() {
        return (
            <MedicationPlanList userModelState={userModel.state}
                                medicationPlans={this.state.medicationPlans}
                                onCreate={medicationPlanPresenter.onCreate}
                                onDelete={medicationPlanPresenter.onDelete}/>
        );
    }
}