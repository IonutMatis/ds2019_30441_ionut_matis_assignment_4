import React, {Component} from 'react';
import medicationPlanModel from "../../../model/medicationPlanModel";
import MedicationPlanAddEdit from "../../dumb/medicationPlan/MedicationPlanAddEdit";
import medicationPlanPresenter from "../../../presenter/medicationPlanPresenter";
import userModel from "../../../model/userModel";
import patientModel from "../../../model/patientModel";
import patientPresenter from "../../../presenter/patientPresenter";
import medicationPresenter from "../../../presenter/medicationPresenter";
import medicationModel from "../../../model/medicationModel";

const mapModelStateToComponentState = (medicationPlanModelState) => ({
    editedMedicationPlan: medicationPlanModelState.editedMedicationPlan
});

export default class SmartMedicationPlanEdit extends Component {
    constructor(props) {
        super(props);
        patientPresenter.onInit();
        medicationPresenter.onInit();
        let editedMedicationPlanId = parseInt(props.match.params.id);
        medicationPlanPresenter.onInit()
            .then(() => {
                medicationPlanModel.setEditedMedicationPlan(editedMedicationPlanId);
                this.state = mapModelStateToComponentState(medicationPlanModel.state);
            });
        this.listener = () =>
            this.setState(mapModelStateToComponentState(medicationPlanModel.state));
        medicationPlanModel.addListener("changeMedicationPlan", this.listener);
        patientModel.addListener("changePatient", this.listener);
        medicationModel.addListener("changeMedication", this.listener);

    }

    componentWillUnmount() {
        medicationPlanModel.removeListener("changeMedicationPlan", this.listener);
        patientModel.removeListener("changePatient", this.listener);
        medicationModel.removeListener("changeMedication", this.listener);
    }

    render() {
        return (
            <MedicationPlanAddEdit onChange={medicationPlanPresenter.onEditChange}
                                   onSubmit={medicationPlanPresenter.onEdit}
                                   medicationPlan={medicationPlanModel.state.editedMedicationPlan}
                                   patients={patientModel.state.patients}
                                   allMedications={medicationModel.state.medications}

                                   mainText="Edit a medication plan"
                                   userModelState={userModel.state}/>
        );
    }
}