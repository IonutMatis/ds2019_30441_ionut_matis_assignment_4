import React, {Component} from 'react';
import caregiverModel from "../../../model/caregiverModel";
import CaregiverAddEdit from "../../dumb/caregiver/CaregiverAddEdit";
import caregiverPresenter from "../../../presenter/caregiverPresenter";
import userModel from "../../../model/userModel";

const mapModelStateToComponentState = (caregiverModelState) => ({
    editedCaregiver: caregiverModelState.editedCaregiver
});

export default class SmartCaregiverEdit extends Component {
    constructor(props) {
        super(props);

        let editedCaregiverId = parseInt(props.match.params.id);
        caregiverModel.setEditedCaregiver(editedCaregiverId);
        this.state = mapModelStateToComponentState(caregiverModel.state);
        this.listener = () =>
            this.setState(mapModelStateToComponentState(caregiverModel.state));
        caregiverModel.addListener("changeCaregiver", this.listener);
    }

    componentWillUnmount() {
        caregiverModel.removeListener("changeCaregiver", this.listener);
    }

    render() {
        return (
            <CaregiverAddEdit onChange={caregiverPresenter.onEditChange}
                              onSubmit={caregiverPresenter.onEdit}
                              caregiver={this.state.editedCaregiver}

                              mainText="Edit a caregiver"
                              userModelState={userModel.state}/>
        )
    }
}