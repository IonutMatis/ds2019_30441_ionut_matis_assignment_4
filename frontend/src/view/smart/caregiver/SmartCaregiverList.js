import React, {Component} from "react";
import caregiverModel from "../../../model/caregiverModel";
import CaregiverList from "../../dumb/caregiver/CaregiverList";
import caregiverPresenter from "../../../presenter/caregiverPresenter";
import userModel from "../../../model/userModel";


const mapModelStateToComponentState = (caregiverModelState) => ({
    caregivers: caregiverModelState.caregivers
});

export default class SmartCaregiverList extends Component {
    constructor(props) {
        super(props);

        caregiverPresenter.onInit();
        this.state = mapModelStateToComponentState(caregiverModel.state);
        this.listener = () => this.setState(mapModelStateToComponentState(caregiverModel.state));
        caregiverModel.addListener("changeCaregiver", this.listener);
    }

    componentWillUnmount() {
        caregiverModel.removeListener("changeCaregiver", this.listener);
    }

    render() {
        return (
            <CaregiverList userModelState={userModel.state}
                           caregivers={this.state.caregivers}
                           onCreate={caregiverPresenter.onCreate}
                           onDelete={caregiverPresenter.onDelete}/>
        );
    }
}