import React, {Component} from "react";
import caregiverModel from "../../../model/caregiverModel";
import CaregiverPatientList from "../../dumb/caregiver/CaregiverPatientsList";
import caregiverPresenter from "../../../presenter/caregiverPresenter";
import userModel from "../../../model/userModel";


const mapModelStateToComponentState = (caregiverModelState) => ({
    patients: caregiverModelState.patients
});

export default class SmartCaregiverList extends Component {
    constructor(props) {
        super(props);
        caregiverPresenter.onLoadPatients();

        this.state = mapModelStateToComponentState(caregiverModel.state);
        this.listener = () => this.setState(mapModelStateToComponentState(caregiverModel.state));
        caregiverModel.addListener("changeCaregiver", this.listener);
    }

    componentWillUnmount() {
        caregiverModel.removeListener("changeCaregiver", this.listener);
    }

    render() {
        return (
            <CaregiverPatientList userModelState={userModel.state}
                           patients={this.state.patients}/>
        );
    }
}