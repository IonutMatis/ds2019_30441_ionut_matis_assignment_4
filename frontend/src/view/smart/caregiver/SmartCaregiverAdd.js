import React, {Component} from 'react';
import caregiverModel from "../../../model/caregiverModel";
import CaregiverAddEdit from "../../dumb/caregiver/CaregiverAddEdit";
import caregiverPresenter from "../../../presenter/caregiverPresenter";
import userModel from "../../../model/userModel";

const mapModelStateToComponentState = (caregiverModelState) => ({
    newCaregiver: caregiverModelState.newCaregiver
});

export default class SmartCaregiverAdd extends Component {
    constructor(props) {
        super(props);

        this.state = mapModelStateToComponentState(caregiverModel.state);
        this.listener = (caregiverModelState) =>
            this.setState(mapModelStateToComponentState(caregiverModelState));
        caregiverModel.addListener("changeCaregiver", this.listener);
    }

    componentWillUnmount() {
        caregiverModel.removeListener("changeCaregiver", this.listener);
    }

    render() {
        return (
            <CaregiverAddEdit onChange={caregiverPresenter.onChange}
                              onSubmit={caregiverPresenter.onCreate}
                              caregiver={this.state.newCaregiver}

                              mainText="Add a caregiver"
                              userModelState={userModel.state}/>
        )
    }
}