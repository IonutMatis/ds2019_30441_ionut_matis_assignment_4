import React, {Component} from "react";
import patientModel from "../../../model/patientModel";
import patientPresenter from "../../../presenter/patientPresenter";
import userModel from "../../../model/userModel";
import PatientAccountView from "../../dumb/patient/PatientAccountView";

const mapModelStateToComponentState = (patientModelState) => ({
    currentPatient: patientModelState.currentPatient
});

export default class SmartPatientAccountView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentPatient: {
                name: "",
                birthDate: "",
                gender: "",
                address: "",
                caregiver: null,
            }
        };

        patientPresenter.onLoadPatient()
            .then(() => this.state = mapModelStateToComponentState(patientModel.state));


        this.listener = () => this.setState(mapModelStateToComponentState(patientModel.state));
        patientModel.addListener("changePatient", this.listener);
    }

    componentWillUnmount() {
        patientModel.removeListener("changePatient", this.listener);
    }

    render() {
        return (
            <PatientAccountView userModelState={userModel.state}
                                patient={this.state.currentPatient}/>
        );
    }
}