import React, {Component} from "react";
import patientModel from "../../../model/patientModel";
import PatientList from "../../dumb/patient/PatientList";
import patientPresenter from "../../../presenter/patientPresenter";
import userModel from "../../../model/userModel";
import caregiverPresenter from "../../../presenter/caregiverPresenter";

const mapModelStateToComponentState = (patientModelState) => ({
    patients: patientModelState.patients
});

export default class SmartPatientList extends Component {
    constructor(props) {
        super(props);
        patientPresenter.onInit();
        caregiverPresenter.onInit();

        this.state = mapModelStateToComponentState(patientModel.state);
        this.listener = () => this.setState(mapModelStateToComponentState(patientModel.state));
        patientModel.addListener("changePatient", this.listener);
    }

    componentWillUnmount() {
        patientModel.removeListener("changePatient", this.listener);
    }

    render() {
        return (
            <PatientList userModelState={userModel.state}
                         patients={this.state.patients}
                         onCreate={patientPresenter.onCreate}
                         onDelete={patientPresenter.onDelete}/>
        );
    }
}