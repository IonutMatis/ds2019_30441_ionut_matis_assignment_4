import React from "react";
import MyNavbar from "../navbar/MyNavbar"
import "../../../style/EntityList.css";
import NotLoggedIn from "../NotLoggedIn";
import ResourceNotFound from "../ResourceNotFound";

const MedicationList = ({userModelState, medications, onDelete}) => (
    <div>
        <MyNavbar userModelState={userModelState}/>
        {
            userModelState.currentUser.role === "anonymous"
                ?
                <NotLoggedIn/>
                :
                userModelState.currentUser.role === "patient"
                    ?
                    <ResourceNotFound/>
                    :
                    <div>
                        <div id="table-header">
                            <h2>All medications</h2>
                            <button className="btn btn-success my-btn"
                                    onClick={() => window.location.assign("#/medications/add")}>
                                Create
                            </button>
                        </div>
                        <table className="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Side effects</th>
                                <th scope="col">Dosage</th>
                                <th scope="col">Edit</th>
                                <th scope="col">Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                medications.map((medication, index) => (
                                    <tr key={index}>
                                        <td>{medication.name}</td>
                                        <td>{medication.sideEffects}</td>
                                        <td>{medication.dosage}</td>

                                        <td>
                                            <button
                                                onClick={() => window.location.assign("#/medications/edit/" + medication.id)}
                                                className="btn btn-info">
                                                Edit
                                            </button>
                                        </td>
                                        <td>
                                            <button className="btn btn-danger"
                                                    onClick={() => onDelete(medication.id)}>
                                                Delete
                                            </button>
                                        </td>
                                    </tr>
                                ))
                            }
                            </tbody>
                        </table>
                    </div>
        }
    </div>
);

export default MedicationList;