import React from 'react';
import MyNavbar from "../navbar/MyNavbar";
import "../../../style/EntityList.css";
import NotLoggedIn from "../NotLoggedIn";
import ResourceNotFound from "../ResourceNotFound";

const MedicationAddEdit = ({mainText, userModelState, onChange, onSubmit, medication}) => (
    <div>
        <MyNavbar userModelState={userModelState}/>
        {
            userModelState.currentUser.role === "anonymous"
                ?
                <NotLoggedIn/>
                :
                userModelState.currentUser.role !== "doctor"
                    ?
                    <ResourceNotFound/>
                    :
                    <div id="patient-add-edit">
                        <h2>{mainText}</h2>
                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label">Name</label>
                            <div className="col-sm-10">
                                <input type="text" className="form-control" placeholder="Name"
                                       onChange={e => onChange("name", e.target.value)}
                                       value={medication.name}/>
                            </div>
                        </div>


                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label">Side effects</label>
                            <div className="col-sm-10">
                                <input type="text" className="form-control"
                                       placeholder="Separated by spaces"
                                       value={medication.sideEffects}
                                       onChange={e => onChange("sideEffects", e.target.value)}/>
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label">Dosage</label>
                            <div className="col-sm-10">
                                <input type="text" className="form-control" placeholder="Dosage"
                                       value={medication.dosage}
                                       onChange={e => onChange("dosage", e.target.value)}/>
                            </div>
                        </div>

                        <div className="form-group row">
                            <div className="col-sm-10">
                                <button type="submit" className="btn btn-primary my-btn"
                                        onClick={() => onSubmit()}>
                                    {mainText.split(' ')[0]}
                                </button>
                            </div>
                        </div>
                    </div>
        }
    </div>
);

export default MedicationAddEdit;