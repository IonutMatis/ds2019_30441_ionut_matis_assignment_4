import React from 'react';
import "../../style/App.css";
import userModel from "../../model/userModel";


const ResourceNotFound = () => (
    <div className="container">
        <div className="row">
            <div className="col-md-12">
                <div className="error-template">
                    <h1>
                        Oops!</h1>
                    <h2>
                        404 Not Found</h2>
                    <div className="error-details">
                        Sorry, an error has occurred. Requested page not found!
                    </div>
                    <div className="error-actions">
                        <button onClick={() => {
                            let role = userModel.state.currentUser.role;
                            if (role === "anonymous")
                                window.location.assign("#/");
                            else {
                                window.location.assign("#/" + role);
                            }
                        }}
                                className="btn btn-primary btn-lg">
                            Take Me Home
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
);

export default ResourceNotFound;