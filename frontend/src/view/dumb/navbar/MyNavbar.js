import React from 'react';
import "../../../style/MyNavbar.css"
import MyNavbarDoctorLinks from "./MyNavbarDoctorLinks";
import MyNavbarPatientLinks from "./MyNavbarPatientLinks";
import MyNavbarCaregiverLinks from "./MyNavbarCaregiverLinks";
import {getWebSocketClient} from "../../../model/userModel";

const MyNavbar = ({userModelState}) => (
    <nav className="navbar navbar-expand-lg navbar-light">
        <img
            src="https://bitbucket.org/utcn_dsrl/react-js-example/raw/4b453ff0a365ae8b53f051165a692af4fe380465/src/commons/images/icon.png"
            width="40"
            height="40" alt="logo"/>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
            {
                userModelState.currentUser.role === "doctor" ?
                    <MyNavbarDoctorLinks userModelState={userModelState}/>
                    :
                    ""
            }
            {
                userModelState.currentUser.role === "patient" ?
                    <MyNavbarPatientLinks userModelState={userModelState}/>
                    :
                    ""
            }
            {
                userModelState.currentUser.role === "caregiver" ?
                    <MyNavbarCaregiverLinks userModelState={userModelState}/>
                    :
                    ""
            }
            <ul className="nav navbar-nav navbar-right">
                <li className="nav-item">
                    {
                        userModelState.currentUser.role === "anonymous"
                            ?
                            <button className="btn btn-sm btn-light"
                                    onClick={() => window.location.assign("#/")}>Log in</button>
                            :
                            <span>
                                Logged in as <strong>{userModelState.currentUser.username}</strong>
                                <button className="btn btn-sm btn-light"
                                        onClick={() => {
                                            userModelState.currentUser.id = -1;
                                            userModelState.currentUser.username = "";
                                            userModelState.currentUser.password = "";
                                            userModelState.currentUser.role = "anonymous";
                                            getWebSocketClient().client.deactivate();
                                            window.location.assign("#/")
                                        }}>Log out
                                </button>
                            </span>
                    }
                </li>
            </ul>
        </div>
    </nav>
);

export default MyNavbar;