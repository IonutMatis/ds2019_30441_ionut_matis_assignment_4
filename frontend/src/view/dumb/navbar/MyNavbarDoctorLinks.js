import React from 'react';

const MyNavbarDoctorLinks = ({userModelState}) => (
    <ul className="navbar-nav mr-auto">
        <li className={"nav-item " + ((window.location.hash.includes(userModelState.currentUser.role)) ? "active" : "")}>
                    <span className="nav-link" onClick={() =>
                        window.location.assign("#/" + userModelState.currentUser.role)}>
                        Home
                    </span>
        </li>
        <li className={"nav-item " + ((window.location.hash.includes('#/patients')) ? "active" : "")}>
                    <span className="nav-link" onClick={() => window.location.assign("#/patients")}>
                        Patients
                    </span>
        </li>
        <li className={"nav-item " + ((window.location.hash.includes('#/caregivers')) ? "active" : "")}>
                <span className="nav-link" onClick={() => window.location.assign("#/caregivers")}>
                    Caregivers
                </span>
        </li>
        <li className={"nav-item " + ((window.location.hash.includes('#/medications')) ? "active" : "")}>
                <span className="nav-link" onClick={() => window.location.assign("#/medications")}>
                    Medications
                </span>
        </li>
        <li className={"nav-item " + ((window.location.hash.includes('#/medication-plans')) ? "active" : "")}>
                <span className="nav-link" onClick={() => window.location.assign("#/medication-plans")}>
                    Medication plans
                </span>
        </li>
    </ul>
);

export default MyNavbarDoctorLinks;