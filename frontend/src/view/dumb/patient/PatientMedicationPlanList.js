import React from "react";
import MyNavbar from "../navbar/MyNavbar"
import "../../../style/EntityList.css";
import NotLoggedIn from "../NotLoggedIn";
import ResourceNotFound from "../ResourceNotFound";

const PatientMedicationPlanList = ({userModelState, medicationPlans}) => (
    <div>
        <MyNavbar userModelState={userModelState}/>
        {
            userModelState.currentUser.role === "anonymous"
                ?
                <NotLoggedIn/>
                :
                userModelState.currentUser.role !== "patient"
                    ?
                    <ResourceNotFound/>
                    :
            <div>
                <div id="table-header">
                    <h2>Medication plans of {userModelState.currentUser.username}</h2>
                </div>
                <table className="table table-hover">
                    <thead>
                    <tr>
                        <th scope="col">Description</th>
                        <th scope="col">Creation date</th>
                        <th scope="col">Doctor</th>
                        <th scope="col">Medications</th>
                        <th scope="col">Intake intervals</th>
                        <th scope="col">Start date</th>
                        <th scope="col">End date</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        medicationPlans.map((medicationPlan, index) => (
                            <tr key={index}>
                                <td>{medicationPlan.description}</td>
                                <td>{medicationPlan.creationDate}</td>
                                <td>{medicationPlan.doctor.name}</td>
                                <td>
                                    {
                                        medicationPlan.medications.map((medication, index2) => (
                                            <p key={index2}>{medication.name}</p>
                                        ))
                                    }
                                </td>

                                <td>{medicationPlan.intakeIntervals}</td>
                                <td>{medicationPlan.startDate}</td>
                                <td>{medicationPlan.endDate}</td>
                            </tr>
                        ))
                    }
                    </tbody>
                </table>
            </div>
        }
    </div>
);

export default PatientMedicationPlanList;