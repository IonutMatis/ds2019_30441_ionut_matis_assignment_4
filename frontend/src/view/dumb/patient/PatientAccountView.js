import React from 'react';
import MyNavbar from "../navbar/MyNavbar";
import "../../../style/EntityList.css";
import NotLoggedIn from "../NotLoggedIn";
import ResourceNotFound from "../ResourceNotFound";

const PatientAccountView = ({userModelState, patient}) => (
    <div>
        <MyNavbar userModelState={userModelState}/>
        {
            userModelState.currentUser.role === "anonymous"
                ?
                <NotLoggedIn/>
                :
                userModelState.currentUser.role !== "patient"
                    ?
                    <ResourceNotFound/>
                    :

                    <div id="patient-add-edit">
                        <h2>Your account details</h2>
                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label">Name</label>
                            <div className="col-sm-10">
                                <input type="text" className="form-control"
                                       disabled={true}
                                       value={patient.name}/>
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label">Birth date</label>
                            <div className="col-sm-10">
                                <input type="date" className="form-control"
                                       disabled={true}
                                       value={patient.birthDate}/>
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label">Gender</label>
                            <div className="col-sm-10">
                                <input type="text" className="form-control"
                                       disabled={true}
                                       value={patient.gender}/>
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label">Address</label>
                            <div className="col-sm-10">
                                <input type="text" className="form-control"
                                       disabled={true}
                                       value={patient.address}/>
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label">Caregiver</label>
                            <div className="col-sm-10">
                                <input type="text" className="form-control"
                                       disabled={true}
                                       value={patient.caregiver === null ? "-" : patient.caregiver.name}/>
                            </div>
                        </div>

                        <div className="form-group row">
                            <div className="col-sm-10">
                                <button type="submit" className="btn btn-primary my-btn"
                                        onClick={() => window.location.assign("#/patient")}>
                                    Back
                                </button>
                            </div>
                        </div>
                    </div>
        }
    </div>
);

export default PatientAccountView;