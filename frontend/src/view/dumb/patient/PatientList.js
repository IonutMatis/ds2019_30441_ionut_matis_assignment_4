import React from "react";
import MyNavbar from "../navbar/MyNavbar"
import "../../../style/EntityList.css";
import NotLoggedIn from "../NotLoggedIn";
import ResourceNotFound from "../ResourceNotFound";

const PatientList = ({userModelState, patients, onDelete}) => (
    <div>
        <MyNavbar userModelState={userModelState}/>
        {
            userModelState.currentUser.role === "anonymous"
                ?
                <NotLoggedIn/>
                :
                userModelState.currentUser.role === "patient"
                    ?
                    <ResourceNotFound/>
                    :
                    <div>
                        <div id="table-header">
                            <h2>All patients</h2>
                            <button className="btn btn-success my-btn"
                                    onClick={() => window.location.assign("#/patients/add")}>
                                Create
                            </button>
                        </div>
                        <table className="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Birth date</th>
                                <th scope="col">Gender</th>
                                <th scope="col">Address</th>
                                <th scope="col">Caregiver</th>
                                <th scope="col">Edit</th>
                                <th scope="col">Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                patients.map((patient, index) => (
                                    <tr key={index}>
                                        <td>{patient.name}</td>
                                        <td>{patient.birthDate}</td>
                                        <td>{patient.gender}</td>
                                        <td>{patient.address}</td>
                                        <td>{patient.caregiver == null
                                            ?
                                            "-"
                                            :
                                            patient.caregiver.name}</td>

                                        <td>
                                            <button
                                                onClick={() => window.location.assign("#/patients/edit/" + patient.id)}
                                                className="btn btn-info">
                                                Edit
                                            </button>
                                        </td>
                                        <td>
                                            <button className="btn btn-danger"
                                                    onClick={() => onDelete(patient.id)}>
                                                Delete
                                            </button>
                                        </td>
                                    </tr>
                                ))
                            }
                            </tbody>
                        </table>
                    </div>
        }
    </div>
);

export default PatientList;