import React from 'react';
import MyNavbar from "../navbar/MyNavbar";
import "../../../style/EntityList.css";
import NotLoggedIn from "../NotLoggedIn";
import ResourceNotFound from "../ResourceNotFound";

const MedicationPlanAddEdit = ({mainText, userModelState, onChange, onSubmit, medicationPlan, patients, allMedications}) => (
        <div>
            <MyNavbar userModelState={userModelState}/>
            {
                userModelState.currentUser.role === "anonymous"
                    ?
                    <NotLoggedIn/>
                    :
                    userModelState.currentUser.role === "caregiver"
                        ?
                        <ResourceNotFound/>
                        :

                        <div id="patient-add-edit">
                            <h2>{mainText}</h2>
                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label">Description</label>
                                <div className="col-sm-10">
                                    <input type="text" className="form-control" placeholder="Description"
                                           onChange={e => onChange("description", e.target.value)}
                                           value={medicationPlan.description}/>
                                </div>
                            </div>

                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label">Patient</label>
                                <div className="col-sm-10">
                                    <select className="custom-select"
                                            onChange={e => onChange("patient", e.target.value)}>
                                        {
                                            patients.map((patient, index) => (
                                                <option key={index}
                                                        value={JSON.stringify({id: patient.id, name: patient.name})}
                                                        selected={medicationPlan.patient.name === patient.name}>
                                                    {patient.name}
                                                </option>
                                            ))
                                        }
                                    </select>
                                </div>
                            </div>


                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label">Intake intervals</label>
                                <div className="col-sm-10">
                                    <input type="text" className="form-control" placeholder="Intake intervals separated by spaces"
                                           value={medicationPlan.intakeIntervals}
                                           onChange={e => onChange("intakeIntervals", e.target.value)}/>
                                </div>
                            </div>

                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label">Medications</label>
                                <div className="col-sm-10">
                                    {
                                        <input type="text" className="form-control"
                                               placeholder="Medications separated by space"
                                               value={medicationPlan.medications}
                                               onChange={e => onChange("medications", e.target.value)}/>
                                    }
                                    Available medications: {allMedications.map(med => med.name).join(", ")}
                                </div>
                            </div>

                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label">Start date</label>
                                <div className="col-sm-10">
                                    <input type="date" className="form-control" placeholder="yyyy-mm-dd"
                                           value={medicationPlan.startDate}
                                           onChange={e => onChange("startDate", e.target.value)}/>
                                </div>
                            </div>

                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label">End date</label>
                                <div className="col-sm-10">
                                    <input type="date" className="form-control" placeholder="yyyy-mm-dd"
                                           value={medicationPlan.endDate}
                                           onChange={e => onChange("endDate", e.target.value)}/>
                                </div>
                            </div>

                            <div className="form-group row">
                                <div className="col-sm-10">
                                    <button type="submit" className="btn btn-primary my-btn"
                                            onClick={() => onSubmit()}>
                                        {mainText.split(' ')[0]}
                                    </button>
                                </div>
                            </div>
                        </div>
            }
        </div>
    )
;

export default MedicationPlanAddEdit;