import React from 'react';
import "../../style/App.css";


const NotLoggedIn = () => (
    <div className="container">
        <div className="row">
            <div className="col-md-12">
                <div className="error-template">
                    <h1>
                        Access denied!
                    </h1>
                    <h2>
                        Not logged in!
                    </h2>
                    <div className="error-details">
                        Please log in before accessing this page.
                    </div>
                    <div className="error-actions">
                        <a href="/" className="btn btn-primary btn-lg">
                            Log in
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
);

export default NotLoggedIn;