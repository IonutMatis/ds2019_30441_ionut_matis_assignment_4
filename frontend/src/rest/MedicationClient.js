export default class MedicationClient {
    constructor(authorization, BASE_URL) {
        this.authorization = authorization;
        this.BASE_URL = BASE_URL;
    }

    loadMedications = () => {
        return fetch(this.BASE_URL + "/medications", {
            method: "GET",
            headers: {
                "Authorization": this.authorization
            }
        }).then(response => response.json())
            .catch((err) => {
                alert("Load medications error");
            });
    };

    addMedication = (name, sideEffects, dosage) => {
        return fetch(this.BASE_URL + "/medications", {
            method: "POST",
            headers: {
                "Authorization": this.authorization,
                "Content-type": "application/json"
            },
            body: JSON.stringify({
                name: name,
                sideEffects: sideEffects,
                dosage: dosage
            })
        }).then(response => {
            if (response.status === 200) {
                return response.json();
            } else {
                alert("Error at creating a medication");
            }
        })
    };

    editMedication(editedMedication) {
        return fetch(this.BASE_URL + "/medications/edit/" + editedMedication.id, {
            method: "PUT",
            headers: {
                "Authorization": this.authorization,
                "Content-type": "application/json"
            },
            body: JSON.stringify({
                name: editedMedication.name,
                sideEffects: editedMedication.sideEffects,
                dosage: editedMedication.dosage
            })
        }).then(response => {
            if (response.status === 200) {
                return response.json();
            } else {
                alert("Error at editing a medication");
            }
        })
    }

    deleteMedication(medicationId) {
        return fetch(this.BASE_URL + "/medications/" + medicationId, {
            method: "DELETE",
            headers: {
                "Authorization": this.authorization,
            }
        }).then(response => {
            if (response.status === 200) {
                return response;
            } else {
                alert("Error at deleting medication");
            }
        })
    }
}