import {EventEmitter} from "events";
import userModel, {getClientFactory} from "./userModel";

class PatientModel extends EventEmitter {
    constructor() {
        super();
        this.state = {
            patients: [],
            newPatient: {
                name: "",
                password: "",
                birthDate: "",
                address: "",
                gender: "MALE",
                caregiver: null
            },
            editedPatient: {
                id: -1,
                name: "",
                password: "",
                birthDate: "",
                gender: "MALE",
                address: "",
                caregiver: {
                    id: -1,
                    name: ""
                }
            },
            medicationPlans: [],
            currentPatient: null
        }
    }

    addPatient() {
        let newPatient = this.state.newPatient;
        if (newPatient.caregiver === '0') {
            newPatient.caregiver = null
        } else {
            newPatient.caregiver = JSON.parse(newPatient.caregiver);
        }
        return getClientFactory().getPatientClient().addPatient(
            newPatient.name,
            newPatient.password,
            newPatient.birthDate,
            newPatient.gender,
            newPatient.address,
            newPatient.caregiver
        ).then((newPatient) => {
            this.emit("changePatient", this.state);
            return newPatient;
        })
    }

    editPatient() {
        let editedPatient = this.state.editedPatient;
        if (editedPatient.caregiver === '0') {
            editedPatient.caregiver = null
        } else {
            editedPatient.caregiver = JSON.parse(editedPatient.caregiver);
        }
        return getClientFactory().getPatientClient().editPatient(editedPatient)
            .then((editedPatient) => {
                this.emit("changePatient", this.state);
                return editedPatient;
            })
    }

    loadPatients() {
        return getClientFactory().getPatientClient().loadPatients()
            .then(patients => {
                this.state = {
                    ...this.state,
                    patients: patients
                };
                this.emit("changePatient", this.state);
                return patients;
            });
    }

    loadPatient() {
        return getClientFactory().getPatientClient().getPatientById(userModel.state.currentUser.id)
            .then(patient => {
                this.state = {
                    ...this.state,
                    currentPatient: patient
                };
                this.emit("changePatient", this.state);
                return patient;
            });
    }

    loadMedicationPlans() {
        return getClientFactory().getPatientClient().getMedicationPlans(userModel.state.currentUser.id)
            .then(medicationPlans => {
                this.state = {
                    ...this.state,
                    medicationPlans: medicationPlans
                };
                this.emit("changePatient", this.state);
                return medicationPlans;
            })
    }

    deletePatient(patientId) {
        getClientFactory().getPatientClient().deletePatient(patientId)
            .then(response => {
                let remainingPatients = this.state.patients.filter(
                    patient => patient.id !== patientId
                );
                this.state = {
                    ...this.state,
                    patients: remainingPatients
                };
                this.emit("changePatient", this.state);
            })
    }

    changeNewPatientProperty(property, value) {
        this.state = {
            ...this.state,
            newPatient: {
                ...this.state.newPatient,
                [property]: value
            }
        };
        this.emit("changePatient", this.state);
    }

    changeEditedPatientProperty(property, value) {
        this.state = {
            ...this.state,
            editedPatient: {
                ...this.state.editedPatient,
                [property]: value
            }
        };
        this.emit("changePatient", this.state);
    }

    resetNewPatientProperties() {
        this.state = {
            ...this.state,
            newPatient: {
                name: "",
                password: "",
                birthDate: "",
                address: "",
                gender: "MALE",
                caregiver: null
            },
        };
        this.emit("changePatient", this.state);
    }

    setEditedPatient(id) {
        let foundPatient = this.state.patients
            .filter(patient => patient.id === id);
        foundPatient[0].password = "";
        this.state = {
            ...this.state,
            editedPatient: foundPatient[0]
        };
        this.emit("changePatient", this.state);
    }
}

const patientModel = new PatientModel();

export default patientModel;