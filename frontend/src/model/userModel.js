import {EventEmitter} from "events";
import ClientFactory from '../rest/ClientFactory';
import WebSocketListener from "../websocket/WebSocketListener";

import { store } from 'react-notifications-component';

let clientFactory = new ClientFactory();
let webSocketClient;

export function getClientFactory() {
    return clientFactory;
}

export function getWebSocketClient() {
    return webSocketClient;
}

class UserModel extends EventEmitter {
    constructor() {
        super();
        this.state = {
            currentUser: {
                id: -1,
                username: "",
                password: "",
                role: "anonymous"
            },
            invalidUsernameOrPassword: null,
        }
    }

    changeUserProperty(property, value) {
        this.state = {
            ...this.state,
            currentUser: {
                ...this.state.currentUser,
                [property]: value
            }
        };
        this.emit("changeUser", this.state);
    }

    changeStateProperty(property, value) {
        this.state = {
            ...this.state,
            [property]: value
        };
        this.emit("changeUser", this.state);
    }

    login() {
        let username = this.state.currentUser.username;
        let password = this.state.currentUser.password;
        return clientFactory.getLoginClient().login(username, password)
            .then(userJson => {
                clientFactory = new ClientFactory(username, password);
                this.changeStateProperty("invalidUsernameOrPassword", false);
                this.changeUserProperty("role", userJson.role.toLowerCase());
                this.changeUserProperty("id", userJson.id);
                if (userJson.role.toLowerCase() === "caregiver") {
                    webSocketClient = new WebSocketListener(userJson.id);
                    webSocketClient.client.activate();
                    this.listenOnWebSocket(webSocketClient);
                }
                this.emit("changeUser", this.state);
            })
            .catch((err) => {
                console.log(err);
                this.changeStateProperty("invalidUsernameOrPassword", true);
                this.emit("changeUser", this.state);
                throw new Error("Invalid username or password!");
            })
    }

    listenOnWebSocket(websocket) {
        websocket.on('patientActivity', patientActivity => {
            console.log(patientActivity);
            if (this.state.currentUser.role === 'caregiver') {
                store.addNotification({
                    title: "Patient activity alert",
                    message: patientActivity.patientName + " spent too much time " + patientActivity.activity,
                    type: "danger",
                    insert: "top",
                    container: "top-right",
                    animationIn: ["animated", "fadeIn"],
                    animationOut: ["animated", "fadeOut"]
                });
            }
        });
    }
}

const userModel = new UserModel();



export default userModel;