package client;

import grpc.GRPCMedicationAction;
import grpc.MedicationPlanRequest;
import grpc.MedicationPlanResponse;
import grpc.PillboxServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class PillboxClient {
    private static final Long PATIENT_ID = 5L;
    private static final String HOST = "localhost";
    private static final int PORT = 9090;

    private PillboxServiceGrpc.PillboxServiceBlockingStub stub;

    public PillboxClient() {
        ManagedChannel channel = ManagedChannelBuilder.forAddress(HOST, PORT)
                .usePlaintext()
                .build();
        stub = PillboxServiceGrpc.newBlockingStub(channel);
    }

    public MedicationPlanResponse downloadMedicationPlans(LocalDate date) {
        return stub.getMedicationPlans(MedicationPlanRequest.newBuilder()
                .setPatientId(PATIENT_ID)
                .setCurrentDate(date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")))
                .build());
    }

    public void sendPatientAction(Long medPlanId, String medicationName, String hour, boolean taken) {
        GRPCMedicationAction.Builder request = GRPCMedicationAction.newBuilder();
        request.setMedicationPlanId(medPlanId)
                .setMedicationName(medicationName)
                .setTime(hour)
                .setTaken(taken);
        stub.sendPatientAction(request.build());
    }
}
