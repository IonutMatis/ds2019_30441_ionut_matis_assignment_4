import controller.MainController;
import model.ProcessRunnable;
import model.TimerRunnable;
import client.PillboxClient;
import view.MainFrame;

public class PillboxApplication {
    public static void main(String[] args) {
        MainFrame mainFrame = new MainFrame();

        PillboxClient pillboxClient = new PillboxClient();
        MainController mainController = new MainController(mainFrame, pillboxClient);

        TimerRunnable globalTimerRunnable = new TimerRunnable(mainFrame, mainController);
        new Thread(globalTimerRunnable).start();

        ProcessRunnable processRunnable = new ProcessRunnable(mainFrame, mainController);
        new Thread(processRunnable).start();
    }
}
