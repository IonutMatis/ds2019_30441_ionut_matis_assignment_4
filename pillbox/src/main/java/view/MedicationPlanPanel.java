package view;

import lombok.Getter;
import utils.MyUtils;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

@Getter
public class MedicationPlanPanel extends JPanel {
    private Long medicationPlanId;
    private List<MedicationPanel> medicationPanels;

    MedicationPlanPanel(Long medicationPlanId) {
        this.medicationPlanId = medicationPlanId;
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        JLabel medicationPlanLabel = new JLabel("Medication plan: " + medicationPlanId);
        medicationPlanLabel.setFont(MyUtils.TNR_BOLD30);
        medicationPlanLabel.setAlignmentX(CENTER_ALIGNMENT);

        this.add(medicationPlanLabel);
        medicationPanels = new ArrayList<>();
    }

    void addMedicationPanel(MedicationPanel medicationPanel) {
        medicationPanels.add(medicationPanel);
        this.add(medicationPanel);
    }
}
