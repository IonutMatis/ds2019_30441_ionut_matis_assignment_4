package view;

import grpc.GRPCMedicationPlan;
import grpc.GRPCMedicationToMedicationPlan;
import lombok.Getter;
import lombok.Setter;
import utils.MyUtils;

import javax.swing.*;
import java.awt.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
public class MainFrame extends JFrame {
    private final static String TITLE = "Pillbox application";
    private final static double PERCENTAGE_SIZE = 0.50;

    private final JPanel contentPanel;

    private JLabel timerLabel;

    private JPanel mainMedicationPlanPanel;
    private List<MedicationPlanPanel> medicationPlanPanels;

    private JTextField currentTimeTextField;
    private JButton increaseTimerSpeedButton;
    private JButton decreaseTimerSpeedButton;
    private JButton resetTimerSpeedButton;

    public MainFrame() {
        contentPanel = new JPanel();
        currentTimeTextField = new JTextField(LocalDateTime.now().format(MyUtils.DATE_TIME_FORMATTER));
        currentTimeTextField.setFont(MyUtils.TNR_PLAIN40);
        currentTimeTextField.setEditable(false);

        increaseTimerSpeedButton = new JButton("+");
        increaseTimerSpeedButton.setFont(MyUtils.TNR_PLAIN40);

        decreaseTimerSpeedButton = new JButton("-");
        decreaseTimerSpeedButton.setFont(MyUtils.TNR_PLAIN40);

        resetTimerSpeedButton = new JButton("Reset speed");
        resetTimerSpeedButton.setFont(MyUtils.TNR_PLAIN40);

        medicationPlanPanels = new ArrayList<>();

        initContentPanel();
        initComponent();
    }

    private void initContentPanel() {
        contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));
        contentPanel.add(Box.createRigidArea(new Dimension(10, 50)));

        contentPanel.add(createTimerPanel());
        contentPanel.add(Box.createRigidArea(new Dimension(10, 50)));

        contentPanel.add(createMedicationPlansPanel());
        contentPanel.add(Box.createRigidArea(new Dimension(10, 300)));

        this.add(contentPanel);
    }

    private JPanel createTimerPanel() {
        JPanel timerPanel = new JPanel();

        timerLabel = new JLabel("Current time: ");
        timerLabel.setFont(MyUtils.TNR_PLAIN40);
        timerPanel.add(timerLabel);

        timerPanel.add(currentTimeTextField);
        timerPanel.add(increaseTimerSpeedButton);
        timerPanel.add(decreaseTimerSpeedButton);
        timerPanel.add(resetTimerSpeedButton);

        return timerPanel;
    }

    private JPanel createMedicationPlansPanel() {
        mainMedicationPlanPanel = new JPanel();
        mainMedicationPlanPanel.setLayout(new BoxLayout(mainMedicationPlanPanel, BoxLayout.Y_AXIS));

        JLabel loadingLabel = new JLabel("Loading medication plans...");
        loadingLabel.setFont(MyUtils.TNR_BOLD40);
        loadingLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        mainMedicationPlanPanel.add(loadingLabel);

        return mainMedicationPlanPanel;
    }

    private void initComponent() {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int width = (int) screenSize.getWidth();
        int height = (int) screenSize.getHeight();

        this.setTitle(TITLE);
        this.setLocation((int) (width * (1.0 - PERCENTAGE_SIZE) / 2), (int) (height * (1.0 - PERCENTAGE_SIZE) / 4));
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setSize((int) (PERCENTAGE_SIZE * width), (int) (PERCENTAGE_SIZE * height * 1.5));
        this.setVisible(true);
    }

    public void updateTimer(String currentTime) {
        currentTimeTextField.setText(currentTime);
        currentTimeTextField.repaint();
    }

    public void updateMedicationPlans(List<GRPCMedicationPlan> medicationPlans) {
        mainMedicationPlanPanel.removeAll();

        for (GRPCMedicationPlan medicationPlan : medicationPlans) {
            MedicationPlanPanel medicationPlanPanel = new MedicationPlanPanel(medicationPlan.getId());

            for (GRPCMedicationToMedicationPlan gRPCMedToMedPlan : medicationPlan.getGRPCMedicationToMedicationPlanList()) {
                MedicationPanel currentMedToMedPlan = new MedicationPanel(gRPCMedToMedPlan);
                medicationPlanPanel.addMedicationPanel(currentMedToMedPlan);
            }

            mainMedicationPlanPanel.add(medicationPlanPanel);
            medicationPlanPanels.add(medicationPlanPanel);
            mainMedicationPlanPanel.add(Box.createRigidArea(new Dimension(30, 40)));
        }

        mainMedicationPlanPanel.revalidate();
        mainMedicationPlanPanel.repaint();
    }

}
