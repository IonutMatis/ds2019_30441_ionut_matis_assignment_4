package model;

import controller.MainController;
import grpc.GRPCMedicationPlan;
import view.MainFrame;

import java.util.List;

public class ProcessRunnable implements Runnable {
    private final MainFrame mainFrame;
    private final MainController mainController;

    private List<GRPCMedicationPlan> medicationPlans;

    public ProcessRunnable(MainFrame mainFrame, MainController mainController) {
        this.mainFrame = mainFrame;
        this.mainController = mainController;
        this.medicationPlans = mainController.downloadMedicationPlans(TimerRunnable.getCurrentTime().toLocalDate());

        mainFrame.updateMedicationPlans(medicationPlans);
        mainController.addClickListenersToTakeButtons();
        mainController.processLogicForButtons();
    }

    @Override
    public void run() {
        while (true) {
            try {
                if (mainController.isDownloadMedicationTime()) {
                    medicationPlans = mainController.downloadMedicationPlans(TimerRunnable.getCurrentTime().toLocalDate());
                    System.out.println("Downloaded " + medicationPlans.size() + " medication plans");
                    mainFrame.updateMedicationPlans(medicationPlans);
                    mainController.addClickListenersToTakeButtons();
                    mainController.processLogicForButtons();
                }

                mainController.processLogicForButtons();

                long sleepTime = mainController.getThreadWaitTime();
                if (sleepTime < 100) {
                    sleepTime = 100;
                }

                Thread.sleep(sleepTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
