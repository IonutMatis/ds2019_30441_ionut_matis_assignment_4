package model;

import controller.MainController;
import lombok.Getter;
import utils.MyUtils;
import view.MainFrame;

import java.time.LocalDateTime;

public class TimerRunnable implements Runnable {
    @Getter
    private final MainFrame mainFrame;

    @Getter
    private final MainController mainController;
    private static LocalDateTime currentTime;

    public TimerRunnable(MainFrame mainFrame, MainController mainController) {
        this.mainFrame = mainFrame;
        this.mainController = mainController;
        currentTime = LocalDateTime.now();
    }

    @Override
    public void run() {
        while (true) {
            try {
                setCurrentTime(getCurrentTime().plusMinutes(1));
                String formattedTime = getCurrentTime().format(MyUtils.DATE_TIME_FORMATTER);

                mainFrame.updateTimer(formattedTime);

                Thread.sleep(mainController.getThreadWaitTime());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static synchronized LocalDateTime getCurrentTime() {
        return currentTime;
    }

    private synchronized void setCurrentTime(LocalDateTime localTime) {
        currentTime = localTime;
    }
}
