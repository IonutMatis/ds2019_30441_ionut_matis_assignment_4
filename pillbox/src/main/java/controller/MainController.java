package controller;

import grpc.GRPCMedicationPlan;
import lombok.Getter;
import model.TimerRunnable;
import client.PillboxClient;
import utils.MyUtils;
import view.MainFrame;
import view.MedicationPanel;
import view.MedicationPlanPanel;

import javax.swing.*;
import java.awt.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Getter
public class MainController {
    private MainFrame mainFrame;
    private Long threadWaitTime;
    private PillboxClient pillboxClient;

    public MainController(MainFrame mainFrame, PillboxClient pillboxClient) {
        this.mainFrame = mainFrame;
        this.pillboxClient = pillboxClient;
        this.threadWaitTime = 1000L;
        addClickListenersRelatedToTime();
    }

    public List<GRPCMedicationPlan> downloadMedicationPlans(LocalDate date) {
        return pillboxClient.downloadMedicationPlans(date).getMedicationPlansList();
    }

    public boolean isDownloadMedicationTime() {
        return TimerRunnable.getCurrentTime().format(MyUtils.HOUR_FORMATTER)
                .equals(MyUtils.DOWNLOAD_MEDICATIONS_TIME);
    }

    private void addClickListenersRelatedToTime() {
        mainFrame.getIncreaseTimerSpeedButton().addActionListener(event -> {
            long decreasedTime = threadWaitTime / 2;
            if (decreasedTime > 0) {
                threadWaitTime = decreasedTime;
            }
        });

        mainFrame.getDecreaseTimerSpeedButton()
                .addActionListener(event -> threadWaitTime = threadWaitTime * 2);

        mainFrame.getResetTimerSpeedButton()
                .addActionListener(event -> resetThreadWaitTime());
    }

    public void addClickListenersToTakeButtons() {
        List<MedicationPlanPanel> medicationPlanPanels = mainFrame.getMedicationPlanPanels();

        for (MedicationPlanPanel medicationPlanPanel : medicationPlanPanels) {
            List<MedicationPanel> medicationPanels = medicationPlanPanel.getMedicationPanels();

            for (MedicationPanel medicationPanel : medicationPanels) {
                JButton takeButton = medicationPanel.getTakeButton();
                takeButton.addActionListener(event -> {
                    Long medPlanId = medicationPlanPanel.getMedicationPlanId();
                    String medicationName = medicationPanel.getMedToMedPlan().getMedicationName();
                    String hour = TimerRunnable.getCurrentTime().format(MyUtils.DATE_TIME_FORMATTER);

                    medicationPanel.setMedicationTaken(true);
                    medicationPanel.setMessageSent(true);
                    takeButton.setText("Taken");
                    takeButton.setBackground(new Color(220, 248, 198));
                    takeButton.setEnabled(false);
                    pillboxClient.sendPatientAction(medPlanId, medicationName, hour, true);
                });
            }
        }
    }

    public void processLogicForButtons() {
        List<MedicationPlanPanel> medicationPlanPanels = mainFrame.getMedicationPlanPanels();

        for (MedicationPlanPanel medicationPlanPanel : medicationPlanPanels) {
            List<MedicationPanel> medicationPanels = medicationPlanPanel.getMedicationPanels();

            for (MedicationPanel medicationPanel : medicationPanels) {
                JButton takeButton = medicationPanel.getTakeButton();
                String[] interval = medicationPanel.getMedToMedPlan().getIntakeInterval().split("-");
                LocalTime start = LocalTime.parse(interval[0], MyUtils.HOUR_FORMATTER);
                LocalTime end = LocalTime.parse(interval[1], MyUtils.HOUR_FORMATTER);
                LocalTime now = TimerRunnable.getCurrentTime().toLocalTime();

                if (now.isBefore(start)) {
                    takeButton.setText("Take");
                    takeButton.setBackground(new Color(0, 0, 0, 21));
                    takeButton.setEnabled(false);
                } else {
                    if (now.isAfter(end)) {
                        if (!medicationPanel.isMedicationTaken()) {
                            takeButton.setBackground(new Color(255, 0, 0, 62));
                            takeButton.setText("Not taken");
                            takeButton.setEnabled(false);
                            if (!medicationPanel.isMessageSent()) {
                                Long medPlanId = medicationPlanPanel.getMedicationPlanId();
                                String medicationName = medicationPanel.getMedToMedPlan().getMedicationName();
                                String hour = TimerRunnable.getCurrentTime().format(MyUtils.DATE_TIME_FORMATTER);

                                medicationPanel.setMessageSent(true);
                                pillboxClient.sendPatientAction(medPlanId, medicationName, hour, false);
                            }
                        }
                    } else {
                        if (!medicationPanel.isMedicationTaken()) {
                            takeButton.setBackground(Color.green);
                            takeButton.setText("Take");
                            takeButton.setEnabled(true);
                        }
                    }
                }
                medicationPanel.revalidate();
                medicationPanel.repaint();
            }
        }
    }

    private synchronized void resetThreadWaitTime() {
        this.threadWaitTime = 1000L;
    }
}
