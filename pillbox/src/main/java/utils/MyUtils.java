package utils;

import java.awt.*;
import java.time.format.DateTimeFormatter;

public class MyUtils {
    public final static DateTimeFormatter HOUR_FORMATTER = DateTimeFormatter.ofPattern("HH:mm");
    public final static DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
    public final static String DOWNLOAD_MEDICATIONS_TIME = "00:00";

    public final static Font TNR_PLAIN30 = new Font("Times New Roman", Font.PLAIN, 30);
    public final static Font TNR_PLAIN40 = new Font("Times New Roman", Font.PLAIN, 40);

    public final static Font TNR_BOLD30 = new Font("Times New Roman", Font.BOLD, 30);
    public final static Font TNR_BOLD40 = new Font("Times New Roman", Font.BOLD, 40);
}
