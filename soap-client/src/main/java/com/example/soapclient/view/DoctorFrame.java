package com.example.soapclient.view;

import com.example.soapclient.gateway.PatientGateway;
import com.example.soapclient.model.ActivityRecord;
import com.example.soapclient.model.Doctor;
import com.example.soapclient.utils.MyUtils;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import soapclient.wsdl.GetAllSimplePatientsResponse;
import soapclient.wsdl.SOAPPatientActivity;
import soapclient.wsdl.SOAPPatientMedication;

import javax.swing.*;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import java.awt.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@Component
public class DoctorFrame extends JFrame {
    private final static String TITLE = "SOAP Doctor";
    private final static double PERCENTAGE_SIZE = 0.50;
    private final Doctor doctor;
    private final PatientGateway patientGateway;

    private final JPanel contentPanel;

    private final JLabel welcomeLabel;
    private final JLabel seeInfoAboutPatients;

    private final JPanel patientButtonsPanel;
    private final List<JButton> patientButtons;

    private final JTabbedPane tabbedPane;

    public DoctorFrame(@Autowired Doctor doctor, @Autowired PatientGateway patientGateway) throws HeadlessException {
        this.doctor = doctor;
        this.patientGateway = patientGateway;
        contentPanel = new JPanel();

        welcomeLabel = new JLabel();
        welcomeLabel.setFont(MyUtils.TNR_BOLD30);

        seeInfoAboutPatients = new JLabel("See information about the patients");
        seeInfoAboutPatients.setFont(MyUtils.TNR_BOLD30);

        patientButtonsPanel = new JPanel();
        patientButtons = new ArrayList<>();

        tabbedPane = new JTabbedPane();

        initContentPanel();
        initComponent();
    }

    private void initContentPanel() {
        contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));

        contentPanel.add(welcomeLabel);
        contentPanel.add(Box.createRigidArea(new Dimension(10, 20)));

        contentPanel.add(seeInfoAboutPatients);
        contentPanel.add(Box.createRigidArea(new Dimension(10, 20)));

        patientButtonsPanel.setLayout(new BoxLayout(patientButtonsPanel, BoxLayout.X_AXIS));
        contentPanel.add(patientButtonsPanel);
        contentPanel.add(Box.createRigidArea(new Dimension(10, 20)));

        contentPanel.add(tabbedPane);

        this.add(contentPanel);
    }

    private void initComponent() {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int width = (int) screenSize.getWidth();
        int height = (int) screenSize.getHeight();

        this.setTitle(TITLE);
        this.setLocation((int) (width * (1.0 - PERCENTAGE_SIZE) / 4), (int) (height * (1.0 - PERCENTAGE_SIZE) / 4));
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setSize((int) (PERCENTAGE_SIZE * width * 1.5), (int) (PERCENTAGE_SIZE * height * 1.5));
        this.setVisible(false);
    }

    public void createPatientButtons(GetAllSimplePatientsResponse response) {
        response.getSimplePatientList().forEach(soapPatient -> {
            JButton patientButton = new JButton(soapPatient.getName());
            patientButton.setFont(MyUtils.TNR_PLAIN30);

            patientButtons.add(patientButton);
            patientButtonsPanel.add(patientButton);
        });

        patientButtonsPanel.revalidate();
        patientButtonsPanel.repaint();
    }

    public void createTabs(Map<LocalDate, ActivityRecord> activityRecords, List<SOAPPatientMedication> medications,
                           List<SOAPPatientActivity> soapPatientActivities) {
        tabbedPane.removeAll();

        JPanel activitiesChartPanel = createActivitiesChartPanel(activityRecords);
        JPanel medicationsPanel = createMedicationsPanel(medications);
        JPanel anomalousActivitiesPanel = createAnomalousActivitiesPanel(soapPatientActivities);

        tabbedPane.addTab("Activities chart", new JScrollPane(activitiesChartPanel));
        tabbedPane.addTab("Medications history", medicationsPanel);
        tabbedPane.addTab("Anomalous activities", anomalousActivitiesPanel);

        tabbedPane.revalidate();
        tabbedPane.repaint();
    }

    private JPanel createActivitiesChartPanel(Map<LocalDate, ActivityRecord> activityRecords) {
        Set<LocalDate> dates = activityRecords.keySet();

        JPanel activitiesChartPanel = new JPanel();
        activitiesChartPanel.setLayout(new BoxLayout(activitiesChartPanel, BoxLayout.Y_AXIS));

        for (LocalDate date : dates) {
            JButton activityButton = new JButton("Activities from " + date.toString());
            activityButton.setBorderPainted(false);
            activityButton.setFont(MyUtils.TNR_PLAIN30);
            activityButton.addActionListener(event ->
                    new ActivityPieChart(date.toString(), activityRecords.get(date)));
            activitiesChartPanel.add(activityButton);
        }

        return activitiesChartPanel;
    }

    private JPanel createMedicationsPanel(List<SOAPPatientMedication> medications) {
        JPanel medicationsPanel = new JPanel();
        medicationsPanel.setLayout(new BoxLayout(medicationsPanel, BoxLayout.Y_AXIS));

        String[] columnNames = {"Medication name",
                "Medication plan id",
                "Taken",
                "Time"};

        String[][] tableData = new String[medications.size()][4];

        for (int i = 0; i < medications.size(); i++) {
            tableData[i][0] = medications.get(i).getMedicationName();
            tableData[i][1] = medications.get(i).getMedicationPlanId() + "";
            tableData[i][2] = medications.get(i).isTaken() + "";
            tableData[i][3] = medications.get(i).getTime();
        }

        JTable table = new JTable(tableData, columnNames);
        table.setFont(MyUtils.TNR_PLAIN30);
        table.setRowHeight(30);
        table.setEnabled(false);
        medicationsPanel.add(new JScrollPane(table));

        return medicationsPanel;
    }

    private JPanel createAnomalousActivitiesPanel(List<SOAPPatientActivity> activities) {
        JPanel anomalousActivitiesPanel = new JPanel();
        anomalousActivitiesPanel.setLayout(new BoxLayout(anomalousActivitiesPanel, BoxLayout.Y_AXIS));

        String[] columnNames = {
                "Activity id",
                "Activity name",
                "Patient id",
                "Start time",
                "End time",
                "Anomalous",
                "Normal behaviour",
                "Recommendation",
                "Save"
        };

        List<SOAPPatientActivity> anomalousActivities = activities.stream()
                .filter(SOAPPatientActivity::isAnomalous)
                .collect(Collectors.toList());

        doctor.setAnomalousActivities(anomalousActivities);
        int nrOfAnomalousActivities = anomalousActivities.size();

        Object[][] tableData = new Object[nrOfAnomalousActivities][columnNames.length];

        int j = 0;
        for (SOAPPatientActivity activity : activities) {
            if (activity.isAnomalous()) {
                tableData[j][0] = activity.getId() + "";
                tableData[j][1] = activity.getActivityName();
                tableData[j][2] = activity.getPatientId() + "";
                tableData[j][3] = activity.getStart();
                tableData[j][4] = activity.getEnd();
                tableData[j][5] = activity.isAnomalous() + "";
                tableData[j][6] = activity.isNormalBehaviour() + "";
                tableData[j][7] = activity.getRecommendation().equals("null") ? "" : activity.getRecommendation();
                tableData[j][8] = activity;

                j++;
            }
        }

        JTable table = new JTable(tableData, columnNames);
        table.setFont(MyUtils.TNR_PLAIN30);
        table.setRowHeight(30);

        TableColumn normalBehaviourColumn = table.getColumnModel().getColumn(6);
        JComboBox<String> comboBox = new JComboBox<>();
        comboBox.addItem("true");
        comboBox.addItem("false");
        normalBehaviourColumn.setCellEditor(new DefaultCellEditor(comboBox));

        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table.getColumnModel().getColumn(0).setPreferredWidth(80);
        table.getColumnModel().getColumn(1).setPreferredWidth(120);
        table.getColumnModel().getColumn(2).setPreferredWidth(80);
        table.getColumnModel().getColumn(3).setPreferredWidth(270);
        table.getColumnModel().getColumn(4).setPreferredWidth(270);
        table.getColumnModel().getColumn(5).setPreferredWidth(80);
        table.getColumnModel().getColumn(6).setPreferredWidth(120);
        table.getColumnModel().getColumn(7).setPreferredWidth(300);
        table.getColumnModel().getColumn(8).setPreferredWidth(80);
        table.getColumn("Save").setCellRenderer(new ButtonRenderer());
        table.getColumn("Save").setCellEditor(new ButtonEditor(new JCheckBox(), patientGateway, doctor));

        table.getModel().addTableModelListener(event -> {
            int row = event.getFirstRow();
            int column = event.getColumn();
            TableModel model = (TableModel) event.getSource();
            Object data = model.getValueAt(row, column);

            // Normal behaviour
            if (column == 6) {
                doctor.getAnomalousActivities().get(row).setNormalBehaviour(Boolean.parseBoolean(data.toString()));
            }

            // Recommendation
            if (column == 7) {
                doctor.getAnomalousActivities().get(row).setRecommendation(data.toString());
            }
        });


        anomalousActivitiesPanel.add(new JScrollPane(table));

        return anomalousActivitiesPanel;
    }
}
