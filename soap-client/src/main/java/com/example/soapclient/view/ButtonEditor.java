package com.example.soapclient.view;

import com.example.soapclient.gateway.PatientGateway;
import com.example.soapclient.model.Doctor;
import soapclient.wsdl.SOAPPatientActivity;

import javax.swing.*;
import java.awt.*;

class ButtonEditor extends DefaultCellEditor {
    private JButton button;
    private PatientGateway patientGateway;
    private Doctor doctor;

    private String label;
    private SOAPPatientActivity activity;

    private boolean isPushed;

    ButtonEditor(JCheckBox checkBox, PatientGateway patientGateway, Doctor doctor) {
        super(checkBox);
        this.patientGateway = patientGateway;
        this.doctor = doctor;
        button = new JButton();
        button.setOpaque(true);
        button.addActionListener(e -> fireEditingStopped());
    }

    public Component getTableCellEditorComponent(JTable table, Object value,
                                                 boolean isSelected, int row, int column) {
        if (isSelected) {
            button.setForeground(table.getSelectionForeground());
            button.setBackground(table.getSelectionBackground());
        } else {
            button.setForeground(table.getForeground());
            button.setBackground(table.getBackground());
        }

        activity = doctor.getActivityById(((SOAPPatientActivity) value).getId());
        patientGateway.updateActivity(activity.getId(), activity.isNormalBehaviour(), activity.getRecommendation());

        label = "Activity " + activity.getId();
        button.setText(label);
        isPushed = true;
        return button;
    }

    public Object getCellEditorValue() {
        if (isPushed) {
            JOptionPane.showMessageDialog(button, label + ": Saved!");
        }
        isPushed = false;
        return activity;
    }

    public boolean stopCellEditing() {
        isPushed = false;
        return super.stopCellEditing();
    }

    protected void fireEditingStopped() {
        super.fireEditingStopped();
    }
}