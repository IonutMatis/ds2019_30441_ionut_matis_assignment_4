package com.example.soapclient.view;

import com.example.soapclient.model.ActivityRecord;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.PieSectionLabelGenerator;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

import javax.swing.*;
import java.text.DecimalFormat;

class ActivityPieChart extends JFrame {

    ActivityPieChart(String title, ActivityRecord activityRecord) {
        super(title);
        PieDataset dataset = createDataset(activityRecord);

        // Create chart
        JFreeChart chart = ChartFactory.createPieChart("Pie Chart of " + title, dataset, true, true, false);

        //Format Label
        PieSectionLabelGenerator labelGenerator = new StandardPieSectionLabelGenerator(
                "{0} : ({2})", new DecimalFormat("0"), new DecimalFormat("0%"));
        ((PiePlot) chart.getPlot()).setLabelGenerator(labelGenerator);

        // Create Panel
        ChartPanel panel = new ChartPanel(chart);
        setContentPane(panel);
        this.setSize(500, 500);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

    private PieDataset createDataset(ActivityRecord activityRecord) {
        DefaultPieDataset dataset = new DefaultPieDataset();

        for (String activity : activityRecord.getActivities().keySet()) {
            dataset.setValue(activity, activityRecord.getActivities().get(activity));
        }

        return dataset;
    }
}
