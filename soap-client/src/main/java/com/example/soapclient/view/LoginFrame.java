package com.example.soapclient.view;

import com.example.soapclient.utils.MyUtils;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;

@Getter
@Setter
@Component
public class LoginFrame extends JFrame {
    private final static String TITLE = "SOAP Client";
    private final static double PERCENTAGE_SIZE = 0.33;

    private final JPanel contentPanel;

    private final JTextField usernameField;
    private final JPasswordField passwordField;
    private final JButton loginButton;

    private JLabel invalidUsernameOrPasswordLabel;

    public LoginFrame() {
        contentPanel = new JPanel();

        usernameField = new JTextField();
        usernameField.setFont(MyUtils.TNR_PLAIN30);
        usernameField.setSize(50, 10);

        passwordField = new JPasswordField();
        passwordField.setFont(MyUtils.TNR_PLAIN30);
        passwordField.setSize(50, 10);

        loginButton = new JButton("Log in");
        loginButton.setFont(MyUtils.TNR_BOLD30);

        invalidUsernameOrPasswordLabel = new JLabel("Invalid username or password!");
        invalidUsernameOrPasswordLabel.setVisible(false);
        invalidUsernameOrPasswordLabel.setForeground(Color.red);

        initContentPanel();
        initComponent();
    }

    private void initContentPanel() {
        contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));

        JLabel loginLabel = new JLabel("Log in");
        loginLabel.setFont(MyUtils.TNR_BOLD40);
        contentPanel.add(loginLabel);
        contentPanel.add(Box.createRigidArea(new Dimension(10, 20)));

        JLabel usernameLabel = new JLabel("Username");
        usernameLabel.setFont(MyUtils.TNR_BOLD30);
        contentPanel.add(usernameLabel);
        contentPanel.add(Box.createRigidArea(new Dimension(10, 10)));

        contentPanel.add(usernameField);
        contentPanel.add(Box.createRigidArea(new Dimension(10, 30)));

        JLabel passwordLabel = new JLabel("Password");
        passwordLabel.setFont(MyUtils.TNR_BOLD30);
        contentPanel.add(passwordLabel);
        contentPanel.add(Box.createRigidArea(new Dimension(10, 10)));

        contentPanel.add(passwordField);
        contentPanel.add(Box.createRigidArea(new Dimension(10, 40)));

        contentPanel.add(invalidUsernameOrPasswordLabel);

        contentPanel.add(loginButton);
        contentPanel.add(Box.createRigidArea(new Dimension(10, 50)));

        this.add(contentPanel);
    }

    private void initComponent() {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int width = (int) screenSize.getWidth();
        int height = (int) screenSize.getHeight();

        this.setTitle(TITLE);
        this.setLocation((int) (width * (1.0 - PERCENTAGE_SIZE) / 5), (int) (height * (1.0 - PERCENTAGE_SIZE) / 3));
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setSize((int) (PERCENTAGE_SIZE * width * 0.75), (int) (PERCENTAGE_SIZE * height * 1.2));
        this.setVisible(true);
    }
}
