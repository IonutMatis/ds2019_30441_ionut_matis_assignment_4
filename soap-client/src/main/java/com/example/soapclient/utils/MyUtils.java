package com.example.soapclient.utils;

import java.awt.*;
import java.time.format.DateTimeFormatter;

public class MyUtils {
    public final static String WS_ENDPOINT = "http://localhost:8081/ws";

    public final static Font TNR_PLAIN30 = new Font("Times New Roman", Font.PLAIN, 30);
    public final static Font TNR_PLAIN40 = new Font("Times New Roman", Font.PLAIN, 40);

    public final static Font TNR_BOLD30 = new Font("Times New Roman", Font.BOLD, 30);
    public final static Font TNR_BOLD40 = new Font("Times New Roman", Font.BOLD, 40);

    public final static DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

}
