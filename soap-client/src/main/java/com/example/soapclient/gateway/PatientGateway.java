package com.example.soapclient.gateway;

import com.example.soapclient.utils.MyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;
import soapclient.wsdl.*;

public class PatientGateway extends WebServiceGatewaySupport {
    private static final Logger log = LoggerFactory.getLogger(LoginGateway.class);
    private static final String WS_GET_ALL_SIMPLE_PATIENTS_REQUEST = "http://example.com/soapserver/soap/getAllSimplePatientsRequest";
    private static final String WS_GET_PATIENT_ACTIVITIES_REQUEST = "http://example.com/soapserver/soap/getPatientActivityHistoryRequest";
    private static final String WS_GET_PATIENT_MEDICATIONS_REQUEST = "http://example.com/soapserver/soap/getPatientMedicationHistoryRequest";
    private static final String WS_UPDATE_PATIENT_ACTIVITY_REQUEST = "http://example.com/soapserver/soap/updateActivityRequest";

    public GetAllSimplePatientsResponse getSimplePatients() {
        GetAllSimplePatientsRequest request = new GetAllSimplePatientsRequest();

        log.info("Requesting \"all simple patients\".");

        GetAllSimplePatientsResponse response = (GetAllSimplePatientsResponse) getWebServiceTemplate()
                .marshalSendAndReceive(MyUtils.WS_ENDPOINT, request,
                        new SoapActionCallback(WS_GET_ALL_SIMPLE_PATIENTS_REQUEST));

        return response;
    }

    public GetPatientActivityHistoryResponse getPatientActivities(Long patientId) {
        GetPatientActivityHistoryRequest request = new GetPatientActivityHistoryRequest();

        log.info("Requesting \"activity history\" of patient " + "\"" + patientId + "\".");

        request.setPatientId(patientId);
        GetPatientActivityHistoryResponse response = (GetPatientActivityHistoryResponse) getWebServiceTemplate()
                .marshalSendAndReceive(MyUtils.WS_ENDPOINT, request,
                        new SoapActionCallback(WS_GET_PATIENT_ACTIVITIES_REQUEST));

        return response;
    }

    public GetPatientMedicationHistoryResponse getPatientAMedications(Long patientId) {
        GetPatientMedicationHistoryRequest request = new GetPatientMedicationHistoryRequest();

        log.info("Requesting \"medication history\" of patient " + "\"" + patientId + "\".");

        request.setPatientId(patientId);
        GetPatientMedicationHistoryResponse response = (GetPatientMedicationHistoryResponse) getWebServiceTemplate()
                .marshalSendAndReceive(MyUtils.WS_ENDPOINT, request,
                        new SoapActionCallback(WS_GET_PATIENT_MEDICATIONS_REQUEST));

        return response;
    }

    public UpdateActivityResponse updateActivity(Long activityId, Boolean normalBehaviour, String recommendation) {
        UpdateActivityRequest request = new UpdateActivityRequest();

        log.info("Updating \"activity\" with id " + "\"" + activityId + "\".");

        SOAPSimplePatientActivity activity = new SOAPSimplePatientActivity();
        activity.setId(activityId);
        activity.setNormalBehaviour(normalBehaviour);
        activity.setRecommendation(recommendation);

        request.setUpdatedActivity(activity);

        UpdateActivityResponse response = (UpdateActivityResponse) getWebServiceTemplate()
                .marshalSendAndReceive(MyUtils.WS_ENDPOINT, request,
                        new SoapActionCallback(WS_UPDATE_PATIENT_ACTIVITY_REQUEST));

        return response;
    }
}
