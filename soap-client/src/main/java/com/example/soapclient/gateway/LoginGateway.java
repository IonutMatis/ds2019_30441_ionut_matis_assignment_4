package com.example.soapclient.gateway;

import com.example.soapclient.utils.MyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;
import soapclient.wsdl.LoginRequest;
import soapclient.wsdl.LoginResponse;

public class LoginGateway extends WebServiceGatewaySupport {
    private static final Logger log = LoggerFactory.getLogger(LoginGateway.class);
    private static final String WS_LOGIN_REQUEST = "http://example.com/soapserver/soap/loginRequest";

    public LoginResponse login(String username, String password) {
        LoginRequest request = new LoginRequest();

        request.setUsername(username);
        request.setPassword(password);
        log.info("Requesting \"login\" for " + "\"" + username + "\".");

        LoginResponse response = (LoginResponse) getWebServiceTemplate()
                .marshalSendAndReceive(MyUtils.WS_ENDPOINT, request,
                        new SoapActionCallback(WS_LOGIN_REQUEST));

        return response;
    }
}
