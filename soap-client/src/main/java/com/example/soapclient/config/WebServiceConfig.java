package com.example.soapclient.config;

import com.example.soapclient.gateway.LoginGateway;
import com.example.soapclient.gateway.PatientGateway;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class WebServiceConfig {
    private final static String WS_URI = "http://localhost:8081/ws";
    private final static String WSDL_PACKAGE = "soapclient.wsdl";

    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        // this package must match the package in the <generatePackage> specified in pom.xml
        marshaller.setContextPath(WSDL_PACKAGE);
        return marshaller;
    }

    @Bean
    public LoginGateway loginGateway(Jaxb2Marshaller marshaller) {
        LoginGateway loginGateway = new LoginGateway();

        loginGateway.setDefaultUri(WS_URI);
        loginGateway.setMarshaller(marshaller);
        loginGateway.setUnmarshaller(marshaller);

        return loginGateway;
    }

    @Bean
    public PatientGateway patientGateway(Jaxb2Marshaller marshaller) {
        PatientGateway patientGateway = new PatientGateway();

        patientGateway.setDefaultUri(WS_URI);
        patientGateway.setMarshaller(marshaller);
        patientGateway.setUnmarshaller(marshaller);

        return patientGateway;
    }
}
