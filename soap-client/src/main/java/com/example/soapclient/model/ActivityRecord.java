package com.example.soapclient.model;

import lombok.Getter;
import lombok.Setter;

import java.util.LinkedHashMap;
import java.util.Map;

@Getter
@Setter
public class ActivityRecord {
    private Map<String, Long> activities;

    public ActivityRecord() {
        activities = new LinkedHashMap<>();
    }

    public void addDurationToActivity(String activityName, Long durationToAdd) {
        activities.putIfAbsent(activityName, 0L);

        Long newDuration = activities.get(activityName);
        newDuration = newDuration + durationToAdd;
        activities.put(activityName, newDuration);
    }
}
