package com.example.soapclient.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import soapclient.wsdl.SOAPPatientActivity;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@Component
public class Doctor {
    private String username;
    private String password;
    private Map<Long, String> patients;
    private List<SOAPPatientActivity> anomalousActivities;

    public Doctor() {
        username = null;
        password = null;
        patients = new LinkedHashMap<>();
        anomalousActivities = new ArrayList<>();
    }

    public Long getPatientId(String patientName) {
        for (Long patientId : patients.keySet()) {
            if (patients.get(patientId).equals(patientName)) {
                return patientId;
            }
        }
        return null;
    }

    public SOAPPatientActivity getActivityById(Long activityId) {
        for (SOAPPatientActivity activity : anomalousActivities) {
            if (activity.getId() == activityId) {
                return activity;
            }
        }

        return null;
    }
}
