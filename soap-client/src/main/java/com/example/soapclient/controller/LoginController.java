package com.example.soapclient.controller;

import com.example.soapclient.gateway.LoginGateway;
import com.example.soapclient.model.Doctor;
import com.example.soapclient.view.DoctorFrame;
import com.example.soapclient.view.LoginFrame;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import soapclient.wsdl.LoginResponse;

import javax.annotation.PostConstruct;
import javax.swing.border.LineBorder;
import java.awt.*;

@Component
@RequiredArgsConstructor
public class LoginController {
    private final LoginFrame loginFrame;
    private final LoginGateway loginGateway;
    private final DoctorFrame doctorFrame;
    private final Doctor doctor;
    private final DoctorController doctorController;

    @PostConstruct
    private void addLoginActionListener() {
        loginFrame.getLoginButton().addActionListener(event -> {
            boolean validFields = true;
            LineBorder redBorder = new LineBorder(Color.red, 2);
            LineBorder blackBorder = new LineBorder(Color.black, 1);

            String username = loginFrame.getUsernameField().getText();
            String password = new String(loginFrame.getPasswordField().getPassword());

            if (username.isEmpty()) {
                loginFrame.getUsernameField().setBorder(redBorder);
                validFields = false;
            } else {
                loginFrame.getUsernameField().setBorder(blackBorder);
            }

            if (password.isEmpty()) {
                loginFrame.getPasswordField().setBorder(redBorder);
                validFields = false;
            } else {
                loginFrame.getPasswordField().setBorder(blackBorder);
            }

            if (validFields) {
                LoginResponse response = loginGateway.login(username, password);

                if (response.getStatus() == 200) {
                    loginFrame.getUsernameField().setBorder(blackBorder);
                    loginFrame.getPasswordField().setBorder(blackBorder);
                    loginFrame.getInvalidUsernameOrPasswordLabel().setVisible(false);
                    doctor.setUsername(username);
                    doctor.setPassword(password);
                    doctorFrame.setVisible(true);
                    loginFrame.setVisible(false);
                    doctorController.loadComponent();
                } else {
                    loginFrame.getUsernameField().setBorder(redBorder);
                    loginFrame.getPasswordField().setBorder(redBorder);
                    loginFrame.getInvalidUsernameOrPasswordLabel().setVisible(true);
                }
            }
        });
    }
}

