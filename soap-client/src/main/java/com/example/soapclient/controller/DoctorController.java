package com.example.soapclient.controller;

import com.example.soapclient.gateway.PatientGateway;
import com.example.soapclient.model.ActivityRecord;
import com.example.soapclient.model.Doctor;
import com.example.soapclient.utils.MyUtils;
import com.example.soapclient.view.DoctorFrame;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import soapclient.wsdl.GetAllSimplePatientsResponse;
import soapclient.wsdl.GetPatientActivityHistoryResponse;
import soapclient.wsdl.GetPatientMedicationHistoryResponse;
import soapclient.wsdl.SOAPPatientActivity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class DoctorController {
    private final DoctorFrame doctorFrame;
    private final Doctor doctor;
    private final PatientGateway patientGateway;

    void loadComponent() {
        doctorFrame.getWelcomeLabel().setText("Welcome, " + doctor.getUsername() + "!");

        GetAllSimplePatientsResponse response = patientGateway.getSimplePatients();
        response.getSimplePatientList()
                .forEach(soapPatient -> doctor.getPatients().put(soapPatient.getId(), soapPatient.getName()));

        doctorFrame.createPatientButtons(response);
        createPatientButtonsActionListeners();
    }

    private void createPatientButtonsActionListeners() {
        doctorFrame.getPatientButtons().forEach(jButton ->
                jButton.addActionListener(event -> {
                    String patientName = jButton.getText();
                    Long patientId = doctor.getPatientId(patientName);

                    GetPatientActivityHistoryResponse activityResponse = patientGateway.getPatientActivities(patientId);
                    Map<LocalDate, ActivityRecord> activityRecords = createDailyActivities(activityResponse);

                    GetPatientMedicationHistoryResponse medicationResponse = patientGateway.getPatientAMedications(patientId);

                    doctorFrame.createTabs(activityRecords, medicationResponse.getPatientMedicationList(), activityResponse.getPatientActivityList());
                }));
    }

    private Map<LocalDate, ActivityRecord> createDailyActivities(GetPatientActivityHistoryResponse response) {
        List<SOAPPatientActivity> activities = response.getPatientActivityList();
        Map<LocalDate, ActivityRecord> activityRecords = new LinkedHashMap<>();

        for (SOAPPatientActivity activity : activities) {
            LocalDateTime startDate = LocalDateTime.parse(activity.getStart(), MyUtils.DATE_TIME_FORMATTER);
            LocalDateTime endDate = LocalDateTime.parse(activity.getEnd(), MyUtils.DATE_TIME_FORMATTER);
            String activityName = activity.getActivityName();

            if (!startDate.toLocalDate().equals(endDate.toLocalDate())) {
                LocalDate start = startDate.toLocalDate();
                LocalDate end = endDate.toLocalDate();

                activityRecords.putIfAbsent(start, new ActivityRecord());
                activityRecords.putIfAbsent(end, new ActivityRecord());

                Long duration1 = ChronoUnit.SECONDS.between(startDate, end.atStartOfDay());
                Long duration2 = ChronoUnit.SECONDS.between(end.atStartOfDay(), endDate);

                activityRecords.get(start).addDurationToActivity(activityName, duration1);
                activityRecords.get(end).addDurationToActivity(activityName, duration2);
            } else {
                LocalDate date = startDate.toLocalDate();
                Long duration = ChronoUnit.SECONDS.between(startDate, endDate);

                activityRecords.putIfAbsent(date, new ActivityRecord());

                ActivityRecord activityRecord = activityRecords.get(date);
                activityRecord.addDurationToActivity(activityName, duration);
            }
        }

        return activityRecords;
    }
}
