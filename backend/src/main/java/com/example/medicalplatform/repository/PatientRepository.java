package com.example.medicalplatform.repository;

import com.example.medicalplatform.model.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Long> {
    @Query(value = "SELECT * FROM USERS WHERE caregiver_id = ?1", nativeQuery = true)
    List<Patient> findByCaregiverId(Long caregiverId);
}
