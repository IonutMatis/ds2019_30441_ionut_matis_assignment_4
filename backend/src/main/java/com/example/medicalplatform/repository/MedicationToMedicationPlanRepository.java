package com.example.medicalplatform.repository;

import com.example.medicalplatform.model.MedicationToMedicationPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicationToMedicationPlanRepository extends JpaRepository<MedicationToMedicationPlan, Long> {
}