package com.example.medicalplatform.controller;

import com.example.medicalplatform.dto.medication.MedicationDTO;
import com.example.medicalplatform.service.MedicationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/medications")
public class MedicationController {
    private final MedicationService medicationService;

    @GetMapping(value = "/{id}")
    public MedicationDTO findById(@PathVariable("id") Long id) {
        return medicationService.findById(id);
    }

    @GetMapping
    public List<MedicationDTO> findAll() {
        return medicationService.findAll();
    }

    @PutMapping("/edit/{id}")
    public MedicationDTO update(@RequestBody MedicationDTO patientCreateUpdateDTO,
                                       @PathVariable("id") Long id) {
        return medicationService.saveUpdate(patientCreateUpdateDTO, id);
    }

    @PostMapping
    public MedicationDTO save(@RequestBody MedicationDTO medicationDTO) {
        return medicationService.saveUpdate(medicationDTO, null);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable("id") Long id) {
        medicationService.deleteById(id);
    }
}
