package com.example.medicalplatform.controller;

import com.example.medicalplatform.dto.medicationPlan.MedicationPlanDTO;
import com.example.medicalplatform.dto.patient.PatientCreateUpdateDTO;
import com.example.medicalplatform.dto.patient.PatientDetailedViewDTO;
import com.example.medicalplatform.dto.patient.PatientSimpleViewDTO;
import com.example.medicalplatform.service.PatientService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/patients")
public class PatientController {
    private final PatientService patientService;

    @GetMapping(value = "/{id}")
    public PatientDetailedViewDTO findById(@PathVariable("id") Long id) {
        return patientService.findById(id);
    }

    @GetMapping
    public List<PatientSimpleViewDTO> findAll() {
        return patientService.findAll();
    }

    @PostMapping
    public PatientSimpleViewDTO save(@RequestBody PatientCreateUpdateDTO patientCreateUpdateDTO) {
        return patientService.saveUpdate(patientCreateUpdateDTO, null);
    }

    @PutMapping("/edit/{id}")
    public PatientSimpleViewDTO update(@RequestBody PatientCreateUpdateDTO patientCreateUpdateDTO,
                                       @PathVariable("id") Long id) {
        return patientService.saveUpdate(patientCreateUpdateDTO, id);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable("id") Long id) {
        patientService.deleteById(id);
    }

    @GetMapping(value = "/{id}/medication-plans")
    public List<MedicationPlanDTO> getMedicationPlansForPatient(@PathVariable("id") Long id) {
        return patientService.getMedicationPlansForPatient(id);
    }
}
