package com.example.medicalplatform.dto.patient;

import com.example.medicalplatform.dto.SimpleUserDTO;
import com.example.medicalplatform.model.Caregiver;
import com.example.medicalplatform.model.Gender;
import com.example.medicalplatform.model.Patient;
import com.example.medicalplatform.model.UserRole;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PatientCreateUpdateDTO {
    private String name;
    private String password;
    private LocalDate birthDate;
    private String gender;
    private String address;
    private SimpleUserDTO<Caregiver> caregiver;

    public static Patient generateModelFromDTO(PatientCreateUpdateDTO dto) {
        Patient patient = new Patient();
        patient.setName(dto.getName());
        patient.setRole(UserRole.PATIENT);
        if (dto.getBirthDate() != null) {
            patient.setBirthDate(dto.getBirthDate());
        } else {
            patient.setBirthDate(LocalDate.now());
        }
        patient.setGender(Gender.valueOf(dto.getGender()));
        patient.setAddress(dto.getAddress());

        return patient;
    }
}
