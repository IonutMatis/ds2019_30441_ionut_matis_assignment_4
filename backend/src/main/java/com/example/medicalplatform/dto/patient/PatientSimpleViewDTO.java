package com.example.medicalplatform.dto.patient;

import com.example.medicalplatform.dto.SimpleUserDTO;
import com.example.medicalplatform.model.Caregiver;
import com.example.medicalplatform.model.Gender;
import com.example.medicalplatform.model.Patient;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PatientSimpleViewDTO {
    private Long id;
    private String name;
    private LocalDate birthDate;
    private String gender;
    private String address;
    private SimpleUserDTO<Caregiver> caregiver;

    public static Patient generateModelFromDTO(PatientSimpleViewDTO dto) {
        Patient patient = new Patient();
        patient.setId(dto.getId());
        patient.setName(dto.getName());
        patient.setBirthDate(dto.getBirthDate());
        patient.setGender(Gender.valueOf(dto.getGender()));
        patient.setAddress(dto.getAddress());

        return patient;
    }

    public static PatientSimpleViewDTO generateDTOFromModel(Patient patient) {
        PatientSimpleViewDTO dto = new PatientSimpleViewDTO();
        dto.setId(patient.getId());
        dto.setName(patient.getName());
        dto.setBirthDate(patient.getBirthDate());
        dto.setGender(patient.getGender().name());
        dto.setAddress(patient.getAddress());
        Caregiver caregiver = patient.getCaregiver();
        if (caregiver != null) {
            dto.setCaregiver(SimpleUserDTO.generateDTOFromModel(patient.getCaregiver()));
        }

        return dto;
    }
}
