package com.example.medicalplatform.dto.patient;

import com.example.medicalplatform.model.Activity;
import com.example.medicalplatform.model.Patient;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PatientActivityDTO {
    private long patientId;
    private String patientName;
    private String activity;

    public static PatientActivityDTO generateDTOFromModels(Patient patient, Activity activity) {
        PatientActivityDTO dto = new PatientActivityDTO();
        dto.setPatientId(patient.getId());
        dto.setPatientName(patient.getName());
        if ("Leaving".equals(activity.getActivity())) {
            dto.setActivity("outdoors");
        } else {
            dto.setActivity(activity.getActivity().toLowerCase());
        }

        return dto;
    }
}
