package com.example.medicalplatform.dto;

import com.example.medicalplatform.model.IdNameEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SimpleUserDTO<USER_MODEL extends IdNameEntity> {
    private Long id;
    private String name;

    public static <USER_MODEL extends IdNameEntity> SimpleUserDTO<USER_MODEL> generateDTOFromModel(USER_MODEL model) {
        SimpleUserDTO<USER_MODEL> dto = new SimpleUserDTO<>();
        dto.setId(model.getEntityId());
        dto.setName(model.getEntityName());

        return dto;
    }
}
