package com.example.medicalplatform.dto.patient;

import com.example.medicalplatform.dto.medicationPlan.MedicationPlanDTO;
import com.example.medicalplatform.model.Patient;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PatientDetailedViewDTO extends PatientSimpleViewDTO {
    private List<MedicationPlanDTO> medicationPlans;

    public static PatientDetailedViewDTO generateDTOFromModel(Patient patient) {
        PatientSimpleViewDTO simpleDTO = PatientSimpleViewDTO.generateDTOFromModel(patient);
        PatientDetailedViewDTO detailedDTO = new PatientDetailedViewDTO();
        PatientDetailedViewDTO.copyProperties(simpleDTO, detailedDTO);
        detailedDTO.setMedicationPlans(patient.getMedicalRecord().stream()
                .map(MedicationPlanDTO::generateDTOFromModel)
                .collect(Collectors.toList()));

        return detailedDTO;
    }

    private static void copyProperties(PatientSimpleViewDTO source, PatientDetailedViewDTO destination) {
        destination.setId(source.getId());
        destination.setName(source.getName());
        destination.setBirthDate(source.getBirthDate());
        destination.setGender(source.getGender());
        destination.setAddress(source.getAddress());
        destination.setCaregiver(source.getCaregiver());
    }
}
