package com.example.medicalplatform.dto.medicationToMedicationPlan;

import com.example.medicalplatform.model.MedicationToMedicationPlan;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MedicationWithIntervalDTO {
    private Long id;
    private String medicationName;
    private String intakeInterval;

    public static MedicationWithIntervalDTO generateDTOFromModel(MedicationToMedicationPlan model) {
        MedicationWithIntervalDTO dto = new MedicationWithIntervalDTO();
        dto.setId(model.getId());
        dto.setMedicationName(model.getMedication().getName());
        dto.setIntakeInterval(model.getIntakeInterval());

        return dto;
    }
}