package com.example.medicalplatform.service.impl;

import com.example.medicalplatform.model.MedicationHistory;
import com.example.medicalplatform.model.MedicationPlan;
import com.example.medicalplatform.model.MedicationToMedicationPlan;
import com.example.medicalplatform.repository.MedicationHistoryRepository;
import com.example.medicalplatform.repository.MedicationPlanRepository;
import com.example.medicalplatform.repository.PatientRepository;
import grpc.*;
import io.grpc.stub.StreamObserver;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class PillboxServiceImpl extends PillboxServiceGrpc.PillboxServiceImplBase {
    private final MedicationPlanRepository medicationPlanRepository;
    private final MedicationHistoryRepository medicationHistoryRepository;

    @Override
    public void getMedicationPlans(MedicationPlanRequest request, StreamObserver<MedicationPlanResponse> responseObserver) {
        Long patientId = request.getPatientId();
        List<MedicationPlan> medicationPlans = medicationPlanRepository.findAll().stream()
                .filter(medicationPlan -> medicationPlan.getPatient().getId().equals(patientId))
                .filter(medicationPlan -> availableMedicationPlan(request.getCurrentDate(), medicationPlan))
                .collect(Collectors.toList());

        MedicationPlanResponse.Builder response = MedicationPlanResponse.newBuilder();

        for (MedicationPlan medicationPlan : medicationPlans) {
            GRPCMedicationPlan.Builder currentMedPlanBuilder = GRPCMedicationPlan.newBuilder();

            currentMedPlanBuilder.setId(medicationPlan.getId());
            currentMedPlanBuilder.setPatientId(medicationPlan.getPatient().getId());

            for (MedicationToMedicationPlan medicationToMedicationPlan : medicationPlan.getMedicationToMedicationPlans()) {
                GRPCMedicationToMedicationPlan.Builder currentMedToMedPlanBuilder = GRPCMedicationToMedicationPlan.newBuilder();
                currentMedToMedPlanBuilder.setId(medicationToMedicationPlan.getId());
                currentMedToMedPlanBuilder.setMedicationName(medicationToMedicationPlan.getMedication().getName());
                currentMedToMedPlanBuilder.setIntakeInterval(medicationToMedicationPlan.getIntakeInterval());

                currentMedPlanBuilder.addGRPCMedicationToMedicationPlan(currentMedToMedPlanBuilder.build());
            }

            currentMedPlanBuilder.setStartDate(medicationPlan.getStartDate().toString());
            currentMedPlanBuilder.setEndDate(medicationPlan.getEndDate().toString());

            response.addMedicationPlans(currentMedPlanBuilder.build());
        }

        responseObserver.onNext(response.build());
        responseObserver.onCompleted();
    }

    @Override
    public void sendPatientAction(GRPCMedicationAction request, StreamObserver<EmptyResponse> responseObserver) {
        Long medicationPlanId = request.getMedicationPlanId();
        String medicationName = request.getMedicationName();
        LocalDateTime time = LocalDateTime.parse(request.getTime(), DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm"));
        boolean taken = request.getTaken();

        medicationHistoryRepository.save(new MedicationHistory(null, medicationPlanId, medicationName, time, taken));

        responseObserver.onNext(EmptyResponse.newBuilder().build());
        responseObserver.onCompleted();
    }

    private boolean availableMedicationPlan(String date, MedicationPlan medicationPlan) {
        LocalDate sendDate = LocalDate.parse(date);
        LocalDate startDate = medicationPlan.getStartDate();
        LocalDate endDate = medicationPlan.getEndDate();

        if (startDate.isEqual(sendDate) || endDate.isEqual(sendDate)) {
            return true;
        }
        return startDate.isBefore(sendDate) && sendDate.isBefore(endDate);
    }
}
