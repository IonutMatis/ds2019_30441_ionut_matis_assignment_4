package com.example.medicalplatform.service.impl;

import com.example.medicalplatform.dto.medication.MedicationDTO;
import com.example.medicalplatform.dto.medication.SimpleMedicationDTO;
import com.example.medicalplatform.dto.medicationPlan.MedicationPlanDTO;
import com.example.medicalplatform.dto.medicationToMedicationPlan.MedicationWithIntervalDTO;
import com.example.medicalplatform.exception.UnknownMedicationException;
import com.example.medicalplatform.exception.UnknownMedicationPlanException;
import com.example.medicalplatform.exception.UnknownPatientException;
import com.example.medicalplatform.model.*;
import com.example.medicalplatform.repository.*;
import com.example.medicalplatform.service.MedicationPlanService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class MedicationPlanServiceImpl implements MedicationPlanService {
    private final MedicationPlanRepository medicationPlanRepository;
    private final PatientRepository patientRepository;
    private final MedicationRepository medicationRepository;
    private final MedicationToMedicationPlanRepository medicationToMedicationPlanRepository;

    @Override
    public MedicationPlanDTO saveUpdate(MedicationPlanDTO dto, Long id) {
        MedicationPlan medicationPlan = MedicationPlanDTO.generateModelFromDTO(dto);

        Patient patient = patientRepository.findById(dto.getPatient().getId())
                .orElseThrow(UnknownPatientException::new);

        MyUserDetails currentUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Doctor doctor = (Doctor) currentUserDetails.getUser();

        Set<MedicationToMedicationPlan> medToMedPlans = new LinkedHashSet<>();
        String[] intakeIntervals = dto.getIntakeIntervals().split(" ");
        Object[] medicationArray = dto.getMedications().toArray(new Object[dto.getMedications().size()]);
        for (int i = 0; i < medicationArray.length; i++) {
            Medication medication = medicationRepository.findByName(((SimpleMedicationDTO) medicationArray[i]).getName())
                    .orElseThrow(UnknownMedicationException::new);
            String intakeInterval = intakeIntervals[i];
            medToMedPlans.add(medicationToMedicationPlanRepository.save(new MedicationToMedicationPlan(dto.getId(), medication, medicationPlan, intakeInterval)));
        }

        medicationPlan.setPatient(patient);
        medicationPlan.setDoctor(doctor);

        medicationPlan.getMedicationToMedicationPlans().addAll(medToMedPlans);

        if (id != null) {
            medicationPlan.setId(id);
        }
        MedicationPlan savedMedicationPlan = medicationPlanRepository.save(medicationPlan);
        medicationToMedicationPlanRepository.saveAll(medToMedPlans);

        return MedicationPlanDTO.generateDTOFromModel(savedMedicationPlan);
    }

    @Override
    public MedicationPlanDTO findById(Long id) {
        MedicationPlan foundMedicationPlan = medicationPlanRepository.findById(id).orElseThrow(UnknownMedicationPlanException::new);

        return MedicationPlanDTO.generateDTOFromModel(foundMedicationPlan);
    }

    @Override
    public List<MedicationPlanDTO> findAll() {
        return medicationPlanRepository.findAll().stream()
                .map(MedicationPlanDTO::generateDTOFromModel)
                .collect(Collectors.toList());
    }

    @Override
    public void deleteById(Long id) {
        MedicationPlan currentMedicationPlan = medicationPlanRepository.findById(id)
                .orElseThrow(UnknownMedicationPlanException::new);
        currentMedicationPlan.getPatient().getMedicalRecord().remove(currentMedicationPlan);
        List<MedicationToMedicationPlan> medToMedPlans = currentMedicationPlan.getMedicationToMedicationPlans();
        medToMedPlans.forEach(medToMedPlan -> {
            medToMedPlan.setMedication(null);
            medToMedPlan.setMedicationPlan(null);
            medicationToMedicationPlanRepository.delete(medToMedPlan);
        });

        currentMedicationPlan.setMedicationToMedicationPlans(null);
        currentMedicationPlan.setDoctor(null);
        currentMedicationPlan.setPatient(null);
        medicationPlanRepository.delete(currentMedicationPlan);
    }
}
