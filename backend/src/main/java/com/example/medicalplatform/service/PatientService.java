package com.example.medicalplatform.service;

import com.example.medicalplatform.dto.medicationPlan.MedicationPlanDTO;
import com.example.medicalplatform.dto.patient.PatientCreateUpdateDTO;
import com.example.medicalplatform.dto.patient.PatientDetailedViewDTO;
import com.example.medicalplatform.dto.patient.PatientSimpleViewDTO;

import java.util.List;

public interface PatientService {
    PatientSimpleViewDTO saveUpdate(PatientCreateUpdateDTO patientCreateUpdateDTO, Long id);

    PatientDetailedViewDTO findById(Long id);

    List<PatientSimpleViewDTO> findAll();

    void deleteById(Long id);

    List<MedicationPlanDTO> getMedicationPlansForPatient(Long id);
}
