package com.example.medicalplatform.service;

import com.example.medicalplatform.model.Activity;

public interface ActivityService {
    void processActivity(Activity activity);
}
