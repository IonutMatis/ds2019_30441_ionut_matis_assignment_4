package com.example.medicalplatform.service.impl;

import com.example.medicalplatform.dto.patient.PatientActivityDTO;
import com.example.medicalplatform.exception.UnknownPatientException;
import com.example.medicalplatform.model.Activity;
import com.example.medicalplatform.model.Caregiver;
import com.example.medicalplatform.model.Patient;
import com.example.medicalplatform.repository.ActivityRepository;
import com.example.medicalplatform.repository.PatientRepository;
import com.example.medicalplatform.service.ActivityService;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
public class ActivityServiceImpl implements ActivityService {
    private final long MS_IN_AN_HOUR = 3600000;
    private final String SLEEPING_ACTIVITY = "Sleeping";
    private final String LEAVING_ACTIVITY = "Leaving";
    private final String TOILETING_ACTIVITY = "Toileting";

    private final PatientRepository patientRepository;
    private final ActivityRepository activityRepository;
    private final SimpMessagingTemplate messagingTemplate;

    @Override
    public void processActivity(Activity activity) {
        activity.setAnomalous(false);

        if (anomalousActivity(activity)) {
            activity.setAnomalous(true);
            System.out.println(activity.toString());
            Patient activityPatient = patientRepository.findById(activity.getPatient_id())
                    .orElseThrow(UnknownPatientException::new);
            Caregiver foundCaregiver = activityPatient.getCaregiver();
            if (foundCaregiver != null) {
                PatientActivityDTO patientData = PatientActivityDTO.generateDTOFromModels(activityPatient, activity);
                messagingTemplate.convertAndSend("/topic/caregiver_" + foundCaregiver.getId(), patientData);
            }
        }

        activityRepository.save(activity);
    }

    private boolean anomalousActivity(Activity activity) {
        if (SLEEPING_ACTIVITY.equals(activity.getActivity())) {
            return activity.getEnd() - activity.getStart() > 12 * MS_IN_AN_HOUR;
        }
        if (LEAVING_ACTIVITY.equals(activity.getActivity())) {
            return activity.getEnd() - activity.getStart() > 12 * MS_IN_AN_HOUR;
        }
        if (TOILETING_ACTIVITY.equals(activity.getActivity())) {
            return activity.getEnd() - activity.getStart() > MS_IN_AN_HOUR;
        }
        return false;
    }
}
