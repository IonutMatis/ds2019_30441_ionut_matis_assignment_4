package com.example.medicalplatform.model;

public enum Gender {
    MALE,
    FEMALE,
    NON_BINARY
}
