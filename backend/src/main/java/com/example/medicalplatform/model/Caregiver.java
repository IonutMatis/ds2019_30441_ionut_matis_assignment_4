package com.example.medicalplatform.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@DiscriminatorValue("CAREGIVER")
public class Caregiver extends User {

    private LocalDate birthDate;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    private String address;

    @OneToMany(mappedBy = "caregiver")
    private List<Patient> patients;

    public Caregiver(String name, String password, LocalDate birthDate, Gender gender, String address, List<Patient> patients) {
        super(null, name, password, UserRole.CAREGIVER);
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.patients = patients;
    }
}
