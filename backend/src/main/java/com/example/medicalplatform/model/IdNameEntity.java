package com.example.medicalplatform.model;

public interface IdNameEntity {
    Long getEntityId();

    String getEntityName();
}
