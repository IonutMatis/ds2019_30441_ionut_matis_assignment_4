package com.example.medicalplatform.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@DiscriminatorValue("PATIENT")
public class Patient extends User {
    private LocalDate birthDate;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    private String address;

    @OneToMany(mappedBy = "patient")
    private List<MedicationPlan> medicalRecord;

    @ManyToOne
    @JoinColumn(name = "caregiver_id")
    private Caregiver caregiver;

    public Patient(Long id) {
        super.setId(id);
    }

    public Patient(String name, String password, LocalDate birthDate, Gender gender, String address, List<MedicationPlan> medicalRecord, Caregiver caregiver) {
        super(null, name, password, UserRole.PATIENT);
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
        this.caregiver = caregiver;
    }
}
